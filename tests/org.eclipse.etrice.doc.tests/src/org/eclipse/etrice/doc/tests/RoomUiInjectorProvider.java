/*******************************************************************************
 * Copyright (c) 2010 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Juergen Haug (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.doc.tests;

import org.eclipse.etrice.core.room.ui.internal.RoomActivator;
import org.eclipse.xtext.testing.IInjectorProvider;

import com.google.inject.Injector;

public class RoomUiInjectorProvider implements IInjectorProvider {

	@Override
	public Injector getInjector() {
		return RoomActivator.getInstance().getInjector("org.eclipse.etrice.core.Room");
	}

}
