RoomModel StringMessageTest {
	
	import etrice.api.types.int8
	import etrice.api.types.int16
	import etrice.api.types.int32
	import etrice.api.types.int64
	import etrice.api.types.float32
	import etrice.api.types.float64
	import etrice.api.types.charPtr
	import etrice.api.language.languageIndicator
	import etrice.api.annotations.TestInstance

	import etrice.api.annotations.StringMessage

	ActorClass SendingStringTop {
		@TestInstance
		Structure {
			ActorRef sender: ASender
			ActorRef receiver: AReceiver
			Binding sender.strPort and receiver.strPort
			
			ActorRef receiver2: AReceiver
			ActorRef receiver3: AReceiver
			
			Binding sender.replStrPort and receiver2.strPort 
			Binding sender.replStrPort and receiver3.strPort 
		}
		Behavior {
		}
	}

	ProtocolClass PString {
		incoming {
			Message strMessage(charPtr) @StringMessage
		}
		outgoing {
			Message strMessage(charPtr) @StringMessage
		}
	}

	ActorClass ASender {
		Interface {
			Port strPort: PString
			Port replStrPort[2]: PString
		}
		Structure {
			external Port strPort
			external Port replStrPort
			
			Attribute caseId: int32
		}
		Behavior {
			StateMachine {
				State state0
				State state1
				State state2
				State state3
				State state4
				State finalState
				Transition init: initial -> state0 {
					action '''
						caseId = etUnit_openAll("log", "StringMessageTest", "org.eclipse.etrice.generator.c.tests.StringMessageTest", "StringMessageTest_case");
						strPort.strMessage("Static string");
					'''
				}
				Transition t0: state0 -> state1 {
					triggers {
						<strMessage: strPort>
					}
					action '''
						ETUNIT_EQUAL_STR("Received incorrect static string", "Static string", transitionData);
						
						char buf[] = "String from buffer";
						strPort.strMessage(buf);
						buf[0] = 'A';
						buf[1] = 'B';
					'''
				}
				Transition t1: state1 -> state2 {
					triggers {
						<strMessage: strPort>
					}
					action '''
						ETUNIT_EQUAL_STR("Received incorrect buffer string", "String from buffer", transitionData);
						
						char buf[] = "String 2 from buffer";
						replStrPort[0].strMessage(buf);
						buf[0] = 'A';
						buf[1] = 'B';
					'''
				}
				
				Transition t2: state2 -> state3 {
					triggers {
						<strMessage: replStrPort>
					}
					action '''
						ETUNIT_EQUAL_STR("Received incorrect buffer string from replicated port", "String 2 from buffer", transitionData);
						
						char buf[] = "String 3 from buffer";
						replStrPort.strMessage(buf);
						buf[0] = 'A';
						buf[1] = 'B';
					'''
				}
				Transition t3: state3 -> state4 {
					triggers {
						<strMessage: replStrPort>
					}
					action '''
						// expect first reply from broadcast
						ETUNIT_EQUAL_STR("Received incorrect buffer string from replicated port", "String 3 from buffer", transitionData);
					'''
				}
				Transition t4: state4 -> finalState {
					triggers {
						<strMessage: replStrPort>
					}
					action '''
						// expect second reply from broadcast
						ETUNIT_EQUAL_STR("Received incorrect buffer string from replicated port", "String 3 from buffer", transitionData);
						
						etUnit_closeAll(caseId);
						etUnit_testFinished(caseId);
					'''
				}
			}
		}
	}

	ActorClass AReceiver {
		Interface {
			conjugated Port strPort: PString
		}
		Structure {
			external Port strPort
		}
		Behavior {
			StateMachine {
				State s0
				Transition init: initial -> s0
				Transition t0: s0 -> s0 {
					triggers {
						<strMessage: strPort>
					}
					action '''
						strPort.strMessage(transitionData);
					'''
				}
			}
		}
	}
	
}