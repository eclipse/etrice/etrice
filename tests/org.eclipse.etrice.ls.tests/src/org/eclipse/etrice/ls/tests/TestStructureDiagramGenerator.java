/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.etrice.core.room.RoomModel;
import org.eclipse.etrice.core.tests.RoomInjectorProvider;
import org.eclipse.etrice.ls.diagram.DiagramGenerator;
import org.eclipse.etrice.ls.diagram.SActorRef;
import org.eclipse.etrice.ls.diagram.SErrorMessage;
import org.eclipse.etrice.ls.diagram.SInterfacePort;
import org.eclipse.etrice.ls.diagram.SInternalPort;
import org.eclipse.etrice.ls.diagram.StructureDiagramGenerator;
import org.eclipse.lsp4j.Position;
import org.eclipse.sprotty.IDiagramState;
import org.eclipse.sprotty.SLabel;
import org.eclipse.sprotty.SModelElement;
import org.eclipse.sprotty.SModelRoot;
import org.eclipse.sprotty.SNode;
import org.eclipse.sprotty.util.IdCache;
import org.eclipse.sprotty.xtext.IDiagramGenerator.Context;
import org.eclipse.sprotty.xtext.ILanguageAwareDiagramServer;
import org.eclipse.sprotty.xtext.SIssueMarkerDecorator;
import org.eclipse.sprotty.xtext.ls.IssueProvider;
import org.eclipse.sprotty.xtext.tracing.ITraceProvider;
import org.eclipse.xtext.ide.server.Document;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;

@RunWith(XtextRunner.class)
@InjectWith(RoomInjectorProvider.class)
public class TestStructureDiagramGenerator {
	@Inject
	private ParseHelper<RoomModel> parseHelper;
	
	private Resource resource;
	private Document document;
	
	private DiagramGenerator diagramGenerator;
	private ITraceProvider traceProvider;
	private SIssueMarkerDecorator issueMarker;
	private EObjectAtOffsetHelper offsetHelper;
	
	@Before
	public void setup() throws Exception {
		String roomFileContent = Files.readString(Path.of("model", "TestVSCodeDiagramGenerator.room"));
		RoomModel roomModel = parseHelper.parse(roomFileContent);
		resource = roomModel.eResource();
		
		// create a document from the room file content to be able to convert line and column positions to offsets (see testCursorPosition)
		// CAREFUL: offsets differ depending on the line endings used, so do not hardcode offsets in tests!
		// alternatively, we could also search for the EObject in the model and then retrieve its offset using an ILocationInFileProvider
		document = new Document(null, roomFileContent);

		traceProvider = mockTraceProvider();
		issueMarker = new SIssueMarkerDecorator();
		offsetHelper = new EObjectAtOffsetHelper();
		diagramGenerator = new DiagramGenerator(traceProvider, issueMarker, offsetHelper);

	}
	
	@Test
	public void testCursorPosition() {
		HashMap<String, String> options = new HashMap<>();
		options.put("diagramType", "structure-diagram");
		SModelRoot graph;
		
		options.put("cursorOffset", Integer.toString(document.getOffSet(new Position(1, 23))));
		graph = diagramGenerator.generate(getContext(options));
		SModelElement errorMsg = graph.getChildren().get(0);
		assertTrue(errorMsg instanceof SErrorMessage);
		
		options.put("cursorOffset", Integer.toString(document.getOffSet(new Position(13, 20))));
		graph = diagramGenerator.generate(getContext(options));
		SModelElement node = graph.getChildren().get(0);
		assertTrue(node instanceof SNode);
		SModelElement label = ((SNode) node).getChildren().get(0);
		assertTrue(label instanceof SLabel);
		assertEquals("BasicTestActor",((SLabel) label).getText());
	}
	
	@Test
	public void testEmptyActor() {
		HashMap<String,String> options = new HashMap<>();
		options.put("diagramType", "structure-diagram");
		options.put("actorName", "EmtpyTestActor");
		
		SModelRoot graph = diagramGenerator.generate(getContext(options));
		assertEquals(1, graph.getChildren().size());
		
		SModelElement element = graph.getChildren().get(0);
		assertTrue(element instanceof SNode);
		SNode node = (SNode) element;
		assertFalse(node.isSelected());
		
		assertEquals(1, node.getChildren().size());
		
	}
	
	@Test
	public void testBasicActor() {
		HashMap<String,String> options = new HashMap<>();
		options.put("diagramType", "structure-diagram");
		options.put("actorName", "BasicTestActor");
		
		SModelRoot graph = diagramGenerator.generate(getContext(options));
		assertEquals(1, graph.getChildren().size());
		
		SModelElement element = graph.getChildren().get(0);
		assertTrue(element instanceof SNode);
		SNode node = (SNode) element;
		
		assertEquals(8, node.getChildren().size());
		
		assertEquals(1, node.getChildren().stream().filter(e -> e.getType() == "label").count());
		assertEquals(3, node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.BINDING_TYPE).count());
		assertEquals(2, node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.ACTOR_REF_TYPE).count());
		assertEquals(2, node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.INTERFACE_PORT_TYPE).count());

		
	}
	
	@Test
	public void testSPPs() {
		HashMap<String,String> options = new HashMap<>();
		options.put("diagramType", "structure-diagram");
		options.put("actorName", "ServiceAccess");
		
		SModelRoot graph = diagramGenerator.generate(getContext(options));
		assertEquals(1, graph.getChildren().size());
		
		SModelElement element = graph.getChildren().get(0);
		assertTrue(element instanceof SNode);
		SNode node = (SNode) element;
		
		assertEquals(4, node.getChildren().size());
		
		assertEquals(1, node.getChildren().stream().filter(e -> e.getType() == "label").count());
		assertEquals(1, node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.SPP_EXTERNAL_TYPE).count());
		assertEquals(1, node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.ACTOR_REF_TYPE).count());
		assertEquals(1, node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.LAYERING_TYPE).count());

		SActorRef actorRef = (SActorRef) node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.ACTOR_REF_TYPE).findAny().orElse(null);
		assertEquals(1, actorRef.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.SPP_REF_TYPE).count());
	}
	
	@Test
	public void testPortTypes() {
		HashMap<String,String> options = new HashMap<>();
		options.put("diagramType", "structure-diagram");
		options.put("actorName", "PortTests");
		
		SModelRoot graph = diagramGenerator.generate(getContext(options));
		assertEquals(1, graph.getChildren().size());

		SModelElement element = graph.getChildren().get(0);
		assertTrue(element instanceof SNode);
		SNode node = (SNode) element;
		
		assertEquals(14, node.getChildren().size());
		
		assertEquals(1, node.getChildren().stream().filter(e -> e.getType() == "label").count());
		assertEquals(2, node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.BINDING_TYPE).count());
		assertEquals(4, node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.INTERNAL_PORT_TYPE).count());
		assertEquals(6, node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.INTERFACE_PORT_TYPE).count());
		assertEquals(1, node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.ACTOR_REF_TYPE).count());
		
		List<SInterfacePort> externalPorts = node.getChildren().stream()
			.filter(e -> e.getType() == StructureDiagramGenerator.INTERFACE_PORT_TYPE).map(e -> (SInterfacePort) e).collect(Collectors.toList());
		assertEquals(3, externalPorts.stream().filter(p -> p.isConjugated()).count());
		assertEquals(2, externalPorts.stream().filter(p -> p.isRelayPort()).count());
		assertEquals(2, externalPorts.stream().filter(p -> p.getMultiplicity() > 1).count());
		
		List<SInternalPort> internalPorts = node.getChildren().stream().filter(e -> e.getType() == StructureDiagramGenerator.INTERNAL_PORT_TYPE).map(e -> (SInternalPort) e).collect(Collectors.toList());
		assertEquals(2, internalPorts.stream().filter(p -> p.isConjugated()).count());
		assertEquals(2, internalPorts.stream().filter(p -> p.getMultiplicity() > 1).count());
		
		
	}
	
	private Context getContext(Map<String,String> options) {
		return new Context(resource, getIDiagramState(options), new IdCache<EObject>(), getIssueProvider(), null);
	}
	private IDiagramState getIDiagramState(Map<String,String> options) {
		return new IDiagramState() {

			@Override
			public Map<String, String> getOptions() {
				return options;
			}

			@Override
			public String getClientId() {
				return "0";
			}

			@Override
			public SModelRoot getCurrentModel() {
				return null;
			}

			@Override
			public Set<String> getExpandedElements() {
				return null;
			}

			@Override
			public Set<String> getSelectedElements() {
				return null;
			}
			
		};
	}
	private IssueProvider getIssueProvider() {
		return new IssueProvider(Collections.emptyList());
	}
	private ITraceProvider mockTraceProvider() {
		return new ITraceProvider() {

			@Override
			public <T extends SModelElement> T trace(T traceable, EObject source) {
				return traceable;
			}

			@Override
			public <T extends SModelElement> T trace(T traceable, EObject source, EStructuralFeature feature,
					int index) {
				return traceable;
			}

			@Override
			public <T> CompletableFuture<T> withSource(SModelElement traceable,
					ILanguageAwareDiagramServer languageServer,
					BiFunction<EObject, org.eclipse.xtext.ide.server.ILanguageServerAccess.Context, T> readOperation) {
				return null;
			}

			@Override
			public SModelElement findSModelElement(SModelRoot root, EObject element) {
				return null;
			}
			
		};
	}
}
 