/*******************************************************************************
 * Copyright (c) 2024 Protos Software GmbH (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Hakan Yesilay (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.etrice.core.room.ActorClass;
import org.junit.Before;
import org.junit.Test;

/*************************
 *       @author 		 *
 *    Hakan Yesilay      * 
 ************************/

public class TestConnectionNecessity extends TestBase {
	private Resource resource;

	@Before
	public void setUp() {
		prepare(CoreTestsActivator.getInstance().getBundle());
		
		resource = getResource("ConnectionNecessity.room");
	}

	@Test
	public void testInternalPortConnection() {
		ActorClass ac = getActorClass("Sender");
		Diagnostic diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"port1\" is not connected", diag.getMessage());
	}
	
	@Test
	public void testRelayPortOfActorRef() {
		ActorClass ac = getActorClass("TopActor1");
		Diagnostic diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"Areceiver.relayPort\" is not connected", diag.getMessage());
	}
	
	@Test
	public void testExternalPortConnection() {	
		ActorClass ac = getActorClass("TopActor2");
		Diagnostic diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"Aactorref.externalPort\" is not connected", diag.getMessage());
	}
	
	@Test
	public void testReplicatedRelayPortConnection() {	
		ActorClass ac = getActorClass("TopActor3");
		Diagnostic diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"AnActor.replicatedRelay\" is not connected", diag.getMessage());
		
		ac = getActorClass("TopActor4");
		diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"AnActor.replicatedRelay\" is connected to 1 peers but its multiplicity is 2. \n"
				+ "For mandatory ports with multiplicity > 1 each port must have a peer.", diag.getMessage());
	
		ac = getActorClass("TopActor5");
		//Check whether there is a warning
		assertTrue("no message expected", getDiag(ac).getChildren().isEmpty());
		//Delete binding to check any warning occurs 
		ac.getBindings().clear(); 
		ac.getActorRefs().remove(0); //Remove one ActorRef to avoid multiple warnings
		diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"AnActor2.externalPort\" is not connected", diag.getMessage());
	}
	
	@Test
	public void testReplicatedExternalPortConnection() {	
		ActorClass ac = getActorClass("TopActor6");
		Diagnostic diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"AnActor2.replicatedExternal\" is not connected", diag.getMessage());
		
		ac = getActorClass("TopActor7");
		diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"AnActor.replicatedExternal\" is connected to 1 peers but its multiplicity is 2. \n"
				+ "For mandatory ports with multiplicity > 1 each port must have a peer.", diag.getMessage());
	
		ac = getActorClass("TopActor8");
		//Check whether there is a warning
		assertTrue("no message expected", getDiag(ac).getChildren().isEmpty());
		//Delete binding to check any warning occurs 
		ac.getBindings().clear();
		ac.getActorRefs().remove(0);  
		diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"AnActor2.replicatedExternalPort\" is not connected", diag.getMessage());
	}
	
	@Test
	public void testReplicatedActorRefMultiplicity() {
		ActorClass ac = getActorClass("TopActor9");
		Diagnostic diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"Aactorref.externalPort\" is not connected", diag.getMessage());
		
		ac = getActorClass("TopActor10");
		diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"AnActorref.externalPort\" is connected to 1 peers but its multiplicity is 2. \n"
				+ "For mandatory ports with multiplicity > 1 each port must have a peer.", diag.getMessage());
		
		ac = getActorClass("TopActor11");
		//Check whether there is a warning
		assertTrue("no message expected", getDiag(ac).getChildren().isEmpty());
		//Delete binding to check any warning occurs 
		ac.getBindings().clear(); 
		ac.getActorRefs().remove(0);
		diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"AnotherActorRef.replicatedRelay\" is not connected", diag.getMessage());
	}
	
	@Test
	public void testOptionalRelayPorts() {
		ActorClass ac = getActorClass("TopActor12");
		Diagnostic diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "Relay port \"relayPort\" is connected to mandatory port \"AnActorref.externalPort\""
				+ " which requires a connection but the relay port is not defined as \"mandatory Port\"", diag.getMessage());
	}
	
	@Test
	public void testBaseClassPorts() {
		ActorClass ac = getActorClass("TopActor13");
		Diagnostic diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"internalPort\" is not connected", diag.getMessage());
		
		ac = getActorClass("TopActor14");
		diag = getDiag(ac).getChildren().get(0);
		assertEquals("expect warning message", "mandatory port \"actorRef.externalPort\" is not connected", diag.getMessage());
	}
	
	private ActorClass getActorClass(String acname) {
		ActorClass ac = (ActorClass) resource.getEObject("ActorClass:" + acname);
		return ac;
	}
}
