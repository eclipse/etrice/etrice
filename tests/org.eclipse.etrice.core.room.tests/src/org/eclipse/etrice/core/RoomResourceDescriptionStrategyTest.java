package org.eclipse.etrice.core;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.etrice.core.resource.RoomResourceDescriptionStrategy;
import org.eclipse.etrice.core.room.RoomClass;
import org.eclipse.etrice.core.room.RoomModel;
import org.eclipse.etrice.core.tests.RoomInjectorProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.util.IAcceptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;

@RunWith(XtextRunner.class)
@InjectWith(RoomInjectorProvider.class)
public class RoomResourceDescriptionStrategyTest extends TestBase {

	private static final String ROOM_NS = "AnnotationExample";
	private static final String ROOM_FILE = ROOM_NS + ".room";

	@Inject
	RoomResourceDescriptionStrategy resourceDescriptionStrategy;

	@Before
	public void setUp() {
		prepare(CoreTestsActivator.getInstance().getBundle());
	}

	@Test
	public void testNotStructureClass() {
		String protocolName = "WrongTargetTestProtocol";
		List<IEObjectDescription> descriptions = createDescriptionsForRoomClass(protocolName, ROOM_FILE);
		assertTrue(descriptions.size() == 1);
		assertNull(getUserDataAnnotationElements(descriptions.get(0)));
	}

	@Test
	public void testNoAnnotations() {
		String actorName = "ANoAnnotations";
		List<IEObjectDescription> descriptions = createDescriptionsForRoomClass(actorName, ROOM_FILE);

		// check description for actor without annotations
		assertTrue(descriptions.size() == 1);
		IEObjectDescription desc = descriptions.get(0);
		assertTrue(desc.getName().equals(QualifiedName.create(ROOM_NS, actorName)));
		assertNull(getUserDataAnnotationElements(desc));
	}

	@Test
	public void testAWithAnnotationAttributes() {
		String actorName = "AWithAnnotationAttributes";
		List<IEObjectDescription> descriptions = createDescriptionsForRoomClass(actorName, ROOM_FILE);

		// check description for actor with annotations
		assertTrue(descriptions.size() == 1);
		IEObjectDescription desc = descriptions.get(0);
		assertTrue(desc.getName().equals(QualifiedName.create(ROOM_NS, actorName)));
		List<String> userDataElements = getUserDataAnnotationElements(desc);

		assertTrue(userDataElements.stream().distinct().count() == 2);

		assertArrayEquals("incorrect types of annotation instances",
				new String[] { "AnnotationExample.WithAttributes", "AnnotationExample.WithAttributes" },
				annotationInstanceTypes(userDataElements).toArray());
	}

	@Test
	public void testAWithAnnotationAttributesInvalid() {
		String actorName = "AWithAnnotationAttributesInvalid";
		List<IEObjectDescription> descriptions = createDescriptionsForRoomClass(actorName, ROOM_FILE);
		IEObjectDescription desc = descriptions.get(0);
		List<String> userDataElements = getUserDataAnnotationElements(desc);

		// attributes that can't be parsed are not included, so attribute hashes
		// expected to be the same
		assertTrue(userDataElements.stream().distinct().count() == 1);

		assertArrayEquals("incorrect types of annotation instances",
				new String[] { "AnnotationExample.WithAttributes", "AnnotationExample.WithAttributes" },
				annotationInstanceTypes(userDataElements).toArray());
	}

	private List<String> getUserDataAnnotationElements(IEObjectDescription desc) {
		String userData = desc.getUserData(RoomResourceDescriptionStrategy.USER_DATA_ROOM_ANNOTATIONS);
		if (userData == null) {
			return null;
		}
		return Arrays.asList(userData.split(","));
	}

	private List<String> annotationInstanceTypes(List<String> elements) {
		return elements.stream().map(element -> element.split(":")[0]).collect(Collectors.toList());
	}

	private List<IEObjectDescription> createDescriptionsForRoomClass(String name, String roomFile) {
		MockEObjectDescriptionAcceptor acceptor = new MockEObjectDescriptionAcceptor();
		Optional<RoomClass> actor = findRoomClass(name, ROOM_FILE);
		resourceDescriptionStrategy.createEObjectDescriptions(actor.orElseThrow(), acceptor);
		return acceptor.getDescriptions();
	}

	private Optional<RoomClass> findRoomClass(String name, String roomFile) {
		Resource res = getResource(roomFile);
		RoomModel model = (RoomModel) res.getContents().get(0);
		Optional<RoomClass> first = roomHelpers.getRoomClasses(model, RoomClass.class)
				.filter(actor -> actor.getName().equals(name)).findFirst();
		return first;
	}

	private static class MockEObjectDescriptionAcceptor implements IAcceptor<IEObjectDescription> {

		private List<IEObjectDescription> descriptions = new ArrayList<IEObjectDescription>();

		@Override
		public void accept(IEObjectDescription desc) {
			descriptions.add(desc);
		}

		public List<IEObjectDescription> getDescriptions() {
			return descriptions;
		}
	}
}
