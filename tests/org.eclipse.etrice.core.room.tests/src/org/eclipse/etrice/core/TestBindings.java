/*******************************************************************************
 * Copyright (c) 2012 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Henrik Rentz-Reichert (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.etrice.core.room.ActorClass;
import org.eclipse.etrice.core.room.ActorContainerRef;
import org.eclipse.etrice.core.room.Binding;
import org.eclipse.etrice.core.room.BindingEndPoint;
import org.eclipse.etrice.core.room.Port;
import org.eclipse.etrice.core.room.RoomFactory;
import org.eclipse.etrice.core.room.RoomModel;
import org.eclipse.etrice.core.room.StructureClass;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Henrik Rentz-Reichert
 *
 */
public class TestBindings extends TestBase {
	private Resource resource;

	@Before
	public void setUp() {
		prepare(CoreTestsActivator.getInstance().getBundle());
		
		resource = getResource("TestBindings.room");
	}

	@Test
	public void testBindingValidation() {
		Binding bind = getBinding("Example1", 0);
		Diagnostic diag = getDiag(bind).getChildren().get(0);
		assertEquals("expect error message", "no self connection allowed, ports are identical", diag.getMessage());
		
		bind = getBinding("Example1a", 0);
		diag = getDiag(bind).getChildren().get(0);
		assertEquals("expect error message", "ports are already bound", diag.getMessage());
		
		bind = getBinding("Example1a", 1);
		diag = getDiag(bind).getChildren().get(0);
		assertEquals("expect error message", "ports are already bound", diag.getMessage());
		
		bind = getBinding("Example2", 0);
		diag = getDiag(bind).getChildren().get(0);
		assertEquals("expect error message", "port with multiplicity 1 is already connected", diag.getMessage());
		
		bind = getBinding("Example2", 0);
		diag = getDiag(bind).getChildren().get(0);
		assertEquals("expect error message", "port with multiplicity 1 is already connected", diag.getMessage());
		
		bind = getBinding("Example3", 0);
		diag = getDiag(bind).getChildren().get(0);
		assertEquals("protocols don't match", diag.getMessage());
		
		bind = getBinding("Example4", 0);
		diag = getDiag(bind).getChildren().get(0);
		assertEquals("protocol extends incoming", diag.getMessage());
		
		bind = getBinding("Example5", 0);
		diag = getDiag(bind);
		assertTrue("binding is ok", diag.getChildren().isEmpty());
		
		bind = getBinding("Example6", 0);
		diag = getDiag(bind).getChildren().get(0);
		assertEquals("protocol extends outgoing", diag.getMessage());
		
		bind = getBinding("Example7", 0);
		diag = getDiag(bind).getChildren().get(0);
		assertEquals("derived protocols not connectable (both directions extended)", diag.getMessage());
	}
	
	@Test
	public void testLocalExternalToLocalRefValidation() {
		ActorClass ac = getActorClass("Example8_external_to_actorref");
		Binding bind = createBinding(createEndPoint(ac, "aref", "reg"), createEndPoint(ac, "external_conj"));
		ac.getBindings().add(bind);
		Diagnostic diag = getDiag(bind).getChildren().get(0);
		assertEquals("expect error message", "external end ports must not be connected", diag.getMessage());
	}
	
	@Test
	public void testInheritedExternalEndPointToLocalRefValidation() {
		ActorClass ac = getActorClass("Example9_inherited_external_to_local_actorref");
		Binding bind = createBinding(createEndPoint(ac, "aref_sub", "reg"), createEndPoint(ac, "external_conj"));
		ac.getBindings().add(bind);
		Diagnostic diag = getDiag(bind).getChildren().get(0);
		assertEquals("expect error message", "external end ports must not be connected", diag.getMessage());
	}

	@Test
	public void testInheritedExternalToInheritedRefValidation() {
		ActorClass ac = getActorClass("Example10_inherited_external_to_inherited_actorref");
		Binding bind = createBinding(createEndPoint(ac, "aref", "reg"), createEndPoint(ac, "external_conj"));
		ac.getBindings().add(bind);
		Diagnostic diag = getDiag(bind).getChildren().get(0);
		assertEquals("expect error message", "external end ports must not be connected", diag.getMessage());
	}
	
	@Test
	public void testProtocolValidation() {
		RoomModel mdl = (RoomModel) resource.getContents().get(0);
		getProtocolClasses(mdl).filter(pc -> pc.getName().equals("PCExtendInOut")).forEach(pc -> {
			Diagnostic diag = getDiag(pc).getChildren().get(0);
			assertEquals("a derived protocol should add either incoming or outgoing messages, not both", diag.getMessage());
		});
	}
	
	private Binding getBinding(String acname, int idx) {
		ActorClass ac = getActorClass(acname);
		return ac.getBindings().get(idx);
	}
	
	private ActorClass getActorClass(String acname) {
		ActorClass ac = (ActorClass) resource.getEObject("ActorClass:"+acname);
		return ac;
	}
	
	private Binding createBinding(BindingEndPoint ep1, BindingEndPoint ep2) {
		Binding bind = RoomFactory.eINSTANCE.createBinding();
		bind.setEndpoint1(ep1);
		bind.setEndpoint2(ep2);
		return bind;
	}
	
	private BindingEndPoint createEndPoint(ActorClass ac, String arefName, String portName) {
		BindingEndPoint ep = RoomFactory.eINSTANCE.createBindingEndPoint();
		ActorContainerRef aref = getRefByName(arefName, ac);
		Port arefPort = getPortByName(portName, aref.getStructureClass());
		ep.setActorRef(aref);
		ep.setPort(arefPort);
		return ep;
	}
	
	private BindingEndPoint createEndPoint(ActorClass ac, String portName) {
		BindingEndPoint ep = RoomFactory.eINSTANCE.createBindingEndPoint();
		Port port = getPortByName(portName, ac);
		ep.setPort(port);
		return ep;
	}
	
	private ActorContainerRef getRefByName(String name, StructureClass sc) {
		ActorContainerRef aref = roomHelpers.getAllActorContainerRefs(sc).stream()
				.filter(it -> it.getName().equals(name))
				.findFirst()
				.orElseThrow();
		return aref;
	}
	
	private Port getPortByName(String name, StructureClass sc) throws ClassCastException {
		ActorClass ac = (ActorClass)sc;
		Port port = roomHelpers.getAllPorts(ac).stream()
				.filter(it -> it.getName().equals(name))
				.findFirst()
				.orElseThrow();
		return port;
	}
}
