# org.eclipse.etrice.releng

The Gradle build in this project assembles the standalone generator distribution.
There is also a very basic test which checks that the standalone generators don't crash at startup.
Run `../../gradlew build` to assemble and check the standalone generators.

The Gradle build also defines some rudimentary helper tasks to assemble third party notices from the output of the Eclipse Dash License Tool.
Executing the following commands in the repository root folder will write them to `build/reports/dash/third-party-notices`:
```sh
mvn org.eclipse.dash:license-tool-plugin:license-check -Dtycho.target.eager=true
./gradlew assembleThirdPartyNotices
```
