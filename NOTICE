# Notices for eTrice

This content is produced and maintained by the Eclipse eTrice project.

 * Project home: https://eclipse.dev/etrice

## Trademarks

Eclipse eTrice and eTrice are trademarks of the Eclipse Foundation. Eclipse,
and the Eclipse Logo are registered trademarks of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
https://www.eclipse.org/legal/epl-v20.html.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

 * https://gitlab.eclipse.org/eclipse/etrice/etrice
 * https://gitlab.eclipse.org/eclipse/etrice/etrice-website

## Third-party Content (Eclipse Plugins)

aopalliance (1.0)

 * License:  LicenseRef-Public-Domain

reload4j (1.2.25)

 * License:  Apache-2.0

jsr305 (3.0.2)

 * License:  Apache-2.0 and CC-BY-2.5

gson (2.10.1)

 * License:  Apache-2.0

error_prone_annotations (2.23.0)

 * License:  Apache-2.0

failureaccess (1.0.2)

 * License:  Apache-2.0

guava (32.1.3-jre)

 * License:  Apache-2.0 AND CC0-1.0 AND LicenseRef-Public-Domain

guava (33.2.0-jre)

 * License:  Apache-2.0 AND CC0-1.0 AND (Apache-2.0 AND CC-PDDC)

listenablefuture (9999.0-empty-to-avoid-conflict-with-guava)

 * License:  Apache-2.0

guice (7.0.0)

 * License:  Apache-2.0

j2objc-annotations (2.8)

 * License:  Apache-2.0

j2objc-annotations (3.0.0)

 * License:  Apache-2.0

icu4j (74.1)

 * License:  Unicode-TOU AND ICU AND BSD-3-Clause AND BSD-2-Clause AND GPL-3.0-or-later WITH Autoconf-exception-2.0 AND NAIST-2003 AND NTP AND LicenseRef-Public-Domain

commons-cli (1.6.0)

 * License:  Apache-2.0

commons-io (2.15.0)

 * License:  Apache-2.0

classgraph (4.8.172)

 * License:  MIT

jakarta.activation-api (1.2.2)

 * License:  EPL-2.0 OR BSD-3-Clause OR GPL-2.0-only with Classpath-exception-2.0

jakarta.annotation-api (2.1.1)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.inject-api (2.0.1)

 * License:  Apache-2.0

jakarta.xml.bind-api (2.3.3)

 * License:  BSD-3-Clause

junit (4.13.2)

 * License:  EPL-2.0

jna-platform (5.13.0)

 * License:  Apache-2.0 OR LGPL-2.1-or-later

jna (5.13.0)

 * License:  Apache-2.0 AND LGPL-2.1-or-later

org.apache.felix.scr (2.2.6)

 * License:  Apache-2.0

apiguardian-api (1.1.2)

 * License:  Apache-2.0

bcpg-jdk18on (1.77)

 * License:  Apache-2.0

bcprov-jdk18on (1.77)

 * License:  MIT AND CC0-1.0

checker-qual (3.37.0)

 * License:  MIT

checker-qual (3.42.0)

 * License:  MIT

animal-sniffer-annotations (1.9)

 * License:  MIT

org.eclipse.lsp4j.jsonrpc (0.21.1)

 * License:  EPL-2.0 OR BSD-3-Clause
 * Project: https://projects.eclipse.org/projects/technology.lsp4j

org.eclipse.lsp4j (0.21.1)

 * License:  EPL-2.0 OR BSD-3-Clause
 * Project: https://projects.eclipse.org/projects/technology.lsp4j

freemarker (2.3.32)

 * License:  Apache-2.0

hamcrest-core (1.3)

 * License:  BSD-2-Clause

hamcrest (2.2)

 * License:  BSD-3-Clause

junit-jupiter-api (5.10.1)

 * License:  EPL-2.0

junit-jupiter-engine (5.10.1)

 * License:  EPL-2.0

junit-jupiter-params (5.10.1)

 * License:  EPL-2.0

junit-platform-commons (1.10.1)

 * License:  EPL-2.0

junit-platform-engine (1.10.1)

 * License:  EPL-2.0

junit-platform-launcher (1.10.1)

 * License:  EPL-2.0

junit-platform-runner (1.10.1)

 * License:  EPL-2.0

junit-platform-suite-api (1.10.1)

 * License:  EPL-2.0

junit-platform-suite-commons (1.10.1)

 * License:  EPL-2.0

opentest4j (1.3.0)

 * License:  Apache-2.0

org.osgi.namespace.extender (1.0.1)

 * License:  Apache-1.1 AND Apache-2.0

org.osgi.namespace.implementation (1.0.0)

 * License:  Apache-1.1 AND Apache-2.0

org.osgi.service.cm (1.6.1)

 * License:  Apache-2.0

org.osgi.service.component (1.5.1)

 * License:  Apache-2.0

org.osgi.service.device (1.1.1)

 * License:  Apache-2.0

org.osgi.service.event (1.4.1)

 * License:  Apache-2.0

org.osgi.service.metatype (1.4.1)

 * License:  Apache-2.0

org.osgi.service.prefs (1.1.2)

 * License:  Apache-2.0

org.osgi.service.provisioning (1.2.0)

 * License:  Apache-1.1 AND Apache-2.0

org.osgi.service.upnp (1.2.1)

 * License:  Apache-2.0

org.osgi.service.useradmin (1.1.1)

 * License:  Apache-2.0

org.osgi.service.wireadmin (1.0.2)

 * License:  Apache-2.0

org.osgi.util.function (1.2.0)

 * License:  Apache-2.0

org.osgi.util.promise (1.3.0)

 * License:  Apache-2.0

osgi.annotation (8.0.1)

 * License:  Apache-2.0

osgi.annotation (8.1.0)

 * License:  Apache-2.0

asm (9.6)

 * License:  BSD-3-Clause

asm (9.7)

 * License:  BSD-3-Clause

xz (1.9)

 * License:  LicenseRef-Public-Domain

org.eclipse.cdt.gdb (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.gnu.build (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.gnu.debug (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.gnu.dsf (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.native (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.platform (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt (11.4.0)

 * License:  EPL-2.0

org.eclipse.license (2.0.2)

 * License:  EPL-2.0

org.eclipse.xtext.redist (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.lib (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

com.google.gson (2.10.1)

 * License:  Apache-2.0

com.sun.jna (5.13.0)

 * License:  Apache-2.0 AND LGPL-2.1-or-later

com.sun.xml.bind (2.3.3)

 * License:  BSD-3-Clause

org.antlr.runtime (3.2.0)

 * License:  BSD-2-Clause

org.aopalliance (1.0.0)

 * License:  Apache-2.0

org.apache.ant (1.10.14)

 * License:  Apache-2.0 AND W3C AND LicenseRef-Public-Domain

org.apache.batik.constants (1.17.0)

 * License:  Apache-2.0

org.apache.batik.css (1.17.0)

 * License:  Apache-2.0

org.apache.batik.i18n (1.17.0)

 * License:  Apache-2.0

org.apache.batik.util (1.17.0)

 * License:  Apache-2.0

org.apache.commons.jxpath (1.3.0)

 * License:  Apache-2.0

org.apache.commons.logging (1.2.0)

 * License:  Apache-2.0

org.apache.xmlgraphics (2.9.0)

 * License:  Apache-2.0

org.commonmark-gfm-strikethrough (0.21.0)

 * License:  BSD-2-Clause
 * Project: https://commonmark.org
 * Attribution: Copyright (c) 2015, Robin Stocker

org.commonmark-gfm-tables (0.21.0)

 * License:  BSD-2-Clause
 * Project: https://commonmark.org
 * Attribution: Copyright (c) 2015, Robin Stocker

org.commonmark-task-list-items (0.21.0)

 * License:  BSD-2-Clause
 * Project: https://commonmark.org
 * Attribution: Copyright (c) 2015, Robin Stocker

org.commonmark (0.21.0)

 * License:  BSD-2-Clause
 * Project: https://commonmark.org
 * Attribution: Copyright (c) 2015, Robin Stocker

org.eclipse.ant.core (3.7.200)

 * License:  EPL-2.0

org.eclipse.ant.launching (1.4.200)

 * License:  EPL-2.0

org.eclipse.ant.ui (3.9.200)

 * License:  EPL-2.0

org.eclipse.cdt.build.gcc.core (2.1.300)

 * License:  EPL-2.0

org.eclipse.cdt.build.gcc.ui (1.2.100)

 * License:  EPL-2.0

org.eclipse.cdt.codan.checkers.ui (3.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.codan.checkers (3.5.400)

 * License:  EPL-2.0

org.eclipse.cdt.codan.core.cxx (3.6.0)

 * License:  EPL-2.0

org.eclipse.cdt.codan.core (4.2.0)

 * License:  EPL-2.0

org.eclipse.cdt.codan.ui.cxx (3.7.0)

 * License:  EPL-2.0

org.eclipse.cdt.codan.ui (3.5.0)

 * License:  EPL-2.0

org.eclipse.cdt.core.linux.x86_64 (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.core.linux (6.1.0)

 * License:  EPL-2.0

org.eclipse.cdt.core.macosx (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.core.native (6.3.200)

 * License:  EPL-2.0

org.eclipse.cdt.core.win32.x86_64 (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.core.win32 (6.1.0)

 * License:  EPL-2.0

org.eclipse.cdt.core (8.3.100)

 * License:  EPL-2.0

org.eclipse.cdt.debug.core (8.8.300)

 * License:  EPL-2.0

org.eclipse.cdt.debug.ui (8.5.400)

 * License:  EPL-2.0

org.eclipse.cdt.doc.user (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.dsf.gdb.ui (2.8.300)

 * License:  EPL-2.0

org.eclipse.cdt.dsf.gdb (7.1.200)

 * License:  EPL-2.0

org.eclipse.cdt.dsf.ui (2.7.200)

 * License:  EPL-2.0

org.eclipse.cdt.dsf (2.12.0)

 * License:  EPL-2.0

org.eclipse.cdt.flatpak.launcher (1.1.100)

 * License:  EPL-2.0

org.eclipse.cdt.gdb.ui (7.2.0)

 * License:  EPL-2.0

org.eclipse.cdt.gdb (7.2.100)

 * License:  EPL-2.0

org.eclipse.cdt.launch (10.4.300)

 * License:  EPL-2.0

org.eclipse.cdt.make.core (7.6.400)

 * License:  EPL-2.0

org.eclipse.cdt.make.ui (8.2.100)

 * License:  EPL-2.0

org.eclipse.cdt.managedbuilder.core (9.6.200)

 * License:  EPL-2.0

org.eclipse.cdt.managedbuilder.gnu.ui (8.6.0)

 * License:  EPL-2.0

org.eclipse.cdt.managedbuilder.headlessbuilderapp (1.1.0)

 * License:  EPL-2.0

org.eclipse.cdt.managedbuilder.ui (9.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.native.serial (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.platform.branding (11.4.0)

 * License:  EPL-2.0

org.eclipse.cdt.ui (8.1.200)

 * License:  EPL-2.0

org.eclipse.cdt (11.4.0)

 * License:  EPL-2.0

org.eclipse.compare.core (3.8.300)

 * License:  EPL-2.0

org.eclipse.compare (3.9.300)

 * License:  EPL-2.0

org.eclipse.core.commands (3.11.200)

 * License:  EPL-2.0

org.eclipse.core.contenttype (3.9.200)

 * License:  EPL-2.0

org.eclipse.core.databinding.beans (1.10.100)

 * License:  EPL-2.0

org.eclipse.core.databinding.observable (1.13.100)

 * License:  EPL-2.0

org.eclipse.core.databinding.property (1.10.100)

 * License:  EPL-2.0

org.eclipse.core.databinding (1.13.100)

 * License:  EPL-2.0

org.eclipse.core.expressions (3.9.200)

 * License:  EPL-2.0

org.eclipse.core.externaltools (1.3.200)

 * License:  EPL-2.0

org.eclipse.core.filebuffers (3.8.200)

 * License:  EPL-2.0

org.eclipse.core.filesystem (1.10.200)

 * License:  EPL-2.0

org.eclipse.core.jobs (3.15.100)

 * License:  EPL-2.0

org.eclipse.core.net (1.5.200)

 * License:  EPL-2.0

org.eclipse.core.resources (3.20.0)

 * License:  EPL-2.0

org.eclipse.core.runtime (3.30.0)

 * License:  EPL-2.0

org.eclipse.core.variables (3.6.200)

 * License:  EPL-2.0

org.eclipse.debug.core (3.21.200)

 * License:  EPL-2.0

org.eclipse.debug.ui (3.18.200)

 * License:  EPL-2.0

org.eclipse.draw2d (3.14.100)

 * License:  EPL-2.0

org.eclipse.e4.core.commands (1.1.200)

 * License:  EPL-2.0

org.eclipse.e4.core.contexts (1.12.400)

 * License:  EPL-2.0

org.eclipse.e4.core.di.annotations (1.8.200)

 * License:  EPL-2.0

org.eclipse.e4.core.di.extensions.supplier (0.17.300)

 * License:  EPL-2.0

org.eclipse.e4.core.di.extensions (0.18.100)

 * License:  EPL-2.0

org.eclipse.e4.core.di (1.9.200)

 * License:  EPL-2.0

org.eclipse.e4.core.services (2.4.200)

 * License:  EPL-2.0

org.eclipse.e4.emf.xpath (0.4.100)

 * License:  EPL-2.0

org.eclipse.e4.ui.bindings (0.14.200)

 * License:  EPL-2.0

org.eclipse.e4.ui.css.core (0.14.200)

 * License:  EPL-2.0

org.eclipse.e4.ui.css.swt.theme (0.14.200)

 * License:  EPL-2.0

org.eclipse.e4.ui.css.swt (0.15.200)

 * License:  EPL-2.0

org.eclipse.e4.ui.di (1.5.200)

 * License:  EPL-2.0

org.eclipse.e4.ui.dialogs (1.4.100)

 * License:  EPL-2.0

org.eclipse.e4.ui.ide (3.17.100)

 * License:  EPL-2.0

org.eclipse.e4.ui.model.workbench (2.4.100)

 * License:  EPL-2.0

org.eclipse.e4.ui.services (1.6.200)

 * License:  EPL-2.0

org.eclipse.e4.ui.widgets (1.4.0)

 * License:  EPL-2.0

org.eclipse.e4.ui.workbench.addons.swt (1.5.200)

 * License:  EPL-2.0

org.eclipse.e4.ui.workbench.renderers.swt (0.16.200)

 * License:  EPL-2.0

org.eclipse.e4.ui.workbench.swt (0.17.200)

 * License:  EPL-2.0

org.eclipse.e4.ui.workbench (1.15.200)

 * License:  EPL-2.0

org.eclipse.e4.ui.workbench3 (0.17.200)

 * License:  EPL-2.0

org.eclipse.emf.codegen.ecore (2.36.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.codegen (2.23.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.common.ui (2.22.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.common (2.29.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.databinding (1.7.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.ecore.change (2.15.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.ecore.edit (2.14.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.ecore.editor (2.18.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.ecore.xmi (2.36.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.ecore (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.edit.ui (2.23.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.edit (2.20.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.mwe.core (1.10.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.mwe.utils (1.10.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.mwe2.language (2.16.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.mwe2.launch (2.16.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.mwe2.lib (2.16.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.mwe2.runtime (2.16.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.transaction (1.9.2)

 * License:  EPL-1.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.validation (1.8.3)

 * License:  EPL-1.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf.workspace (1.5.2)

 * License:  EPL-1.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.equinox.app (1.6.400)

 * License:  EPL-2.0

org.eclipse.equinox.bidi (1.4.400)

 * License:  EPL-2.0

org.eclipse.equinox.common (3.18.200)

 * License:  EPL-2.0

org.eclipse.equinox.event (1.6.300)

 * License:  EPL-2.0

org.eclipse.equinox.frameworkadmin.equinox (1.3.0)

 * License:  EPL-2.0

org.eclipse.equinox.frameworkadmin (2.3.0)

 * License:  EPL-2.0

org.eclipse.equinox.launcher (1.6.600)

 * License:  EPL-2.0

org.eclipse.equinox.p2.artifact.repository (1.5.200)

 * License:  EPL-2.0

org.eclipse.equinox.p2.core (2.10.200)

 * License:  EPL-2.0

org.eclipse.equinox.p2.engine (2.9.0)

 * License:  EPL-2.0

org.eclipse.equinox.p2.jarprocessor (1.3.200)

 * License:  EPL-2.0

org.eclipse.equinox.p2.metadata.repository (1.5.200)

 * License:  EPL-2.0

org.eclipse.equinox.p2.metadata (2.8.0)

 * License:  EPL-2.0

org.eclipse.equinox.p2.repository (2.8.0)

 * License:  EPL-2.0

org.eclipse.equinox.preferences (3.10.400)

 * License:  EPL-2.0

org.eclipse.equinox.registry (3.11.400)

 * License:  EPL-2.0

org.eclipse.equinox.security (1.4.100)

 * License:  EPL-2.0

org.eclipse.equinox.simpleconfigurator.manipulator (2.3.0)

 * License:  EPL-2.0

org.eclipse.equinox.simpleconfigurator (1.5.0)

 * License:  EPL-2.0

org.eclipse.gef (3.16.0)

 * License:  EPL-2.0

org.eclipse.graphiti.mm (0.19.2)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/graphiti

org.eclipse.graphiti.ui (0.19.2)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/graphiti

org.eclipse.graphiti (0.19.2)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/graphiti

org.eclipse.help (3.10.200)

 * License:  EPL-2.0

org.eclipse.jdt.core.compiler.batch (3.36.0)

 * License:  EPL-2.0

org.eclipse.jdt.core.manipulation (1.20.0)

 * License:  EPL-2.0

org.eclipse.jdt.core (3.36.0)

 * License:  EPL-2.0

org.eclipse.jdt.debug.ui (3.13.200)

 * License:  EPL-2.0

org.eclipse.jdt.debug (3.21.200)

 * License:  EPL-2.0

org.eclipse.jdt.junit.core (3.13.0)

 * License:  EPL-2.0

org.eclipse.jdt.junit.runtime (3.7.300)

 * License:  EPL-2.0

org.eclipse.jdt.junit (3.16.200)

 * License:  EPL-2.0

org.eclipse.jdt.launching (3.21.0)

 * License:  EPL-2.0

org.eclipse.jdt.ui (3.31.0)

 * License:  EPL-2.0

org.eclipse.jface.databinding (1.15.100)

 * License:  EPL-2.0

org.eclipse.jface.text (3.24.200)

 * License:  EPL-2.0

org.eclipse.jface (3.32.0)

 * License:  EPL-2.0

org.eclipse.launchbar.core (2.5.100)

 * License:  EPL-2.0

org.eclipse.launchbar.ui (2.5.200)

 * License:  EPL-2.0

org.eclipse.ltk.core.refactoring (3.14.200)

 * License:  EPL-2.0

org.eclipse.ltk.ui.refactoring (3.13.200)

 * License:  EPL-2.0

org.eclipse.nebula.cwt (1.1.0)

 * License:  EPL-2.0

org.eclipse.nebula.widgets.cdatetime (1.5.0)

 * License:  EPL-2.0

org.eclipse.orbit.xml-apis-ext (1.0.0)

 * License:  EPL-2.0

org.eclipse.osgi.services (3.11.200)

 * License:  EPL-2.0

org.eclipse.osgi (3.18.600)

 * License:  EPL-2.0

org.eclipse.search.core (3.16.0)

 * License:  EPL-2.0

org.eclipse.search (3.16.0)

 * License:  EPL-2.0

org.eclipse.swt.cocoa.macosx.x86_64 (3.124.200)

 * License:  EPL-2.0

org.eclipse.swt.gtk.linux.x86_64 (3.124.200)

 * License:  EPL-2.0

org.eclipse.swt.win32.win32.x86_64 (3.124.200)

 * License:  EPL-2.0

org.eclipse.swt (3.124.200)

 * License:  EPL-2.0

org.eclipse.team.core (3.10.200)

 * License:  EPL-2.0

org.eclipse.team.ui (3.10.200)

 * License:  EPL-2.0

org.eclipse.text (3.13.100)

 * License:  EPL-2.0

org.eclipse.tm.terminal.control (5.5.100)

 * License:  EPL-2.0

org.eclipse.tools.templates.core (2.0.0)

 * License:  EPL-2.0

org.eclipse.tools.templates.freemarker (2.0.0)

 * License:  EPL-2.0

org.eclipse.tools.templates.ui (2.0.0)

 * License:  EPL-2.0

org.eclipse.ui.cheatsheets (3.8.200)

 * License:  EPL-2.0

org.eclipse.ui.console (3.13.100)

 * License:  EPL-2.0

org.eclipse.ui.editors (3.17.100)

 * License:  EPL-2.0

org.eclipse.ui.externaltools (3.6.200)

 * License:  EPL-2.0

org.eclipse.ui.forms (3.13.100)

 * License:  EPL-2.0

org.eclipse.ui.ide.application (1.5.200)

 * License:  EPL-2.0

org.eclipse.ui.ide (3.22.0)

 * License:  EPL-2.0

org.eclipse.ui.intro.universal (3.5.200)

 * License:  EPL-2.0

org.eclipse.ui.intro (3.7.200)

 * License:  EPL-2.0

org.eclipse.ui.navigator.resources (3.9.100)

 * License:  EPL-2.0

org.eclipse.ui.navigator (3.12.200)

 * License:  EPL-2.0

org.eclipse.ui.views.properties.tabbed (3.10.100)

 * License:  EPL-2.0

org.eclipse.ui.views (3.12.100)

 * License:  EPL-2.0

org.eclipse.ui.workbench.texteditor (3.17.200)

 * License:  EPL-2.0

org.eclipse.ui.workbench (3.131.0)

 * License:  EPL-2.0

org.eclipse.ui (3.205.0)

 * License:  EPL-2.0

org.eclipse.urischeme (1.3.100)

 * License:  EPL-2.0

org.eclipse.xtend.lib.macro.source (2.35.0)

 * License:  EPL-1.0
 * Project: https://eclipse.dev/Xtext/xtend

org.eclipse.xtend.lib.macro (2.33.0)

 * License:  EPL-1.0
 * Project: https://eclipse.dev/Xtext/xtend

org.eclipse.xtend.lib.macro (2.35.0)

 * License:  EPL-1.0
 * Project: https://eclipse.dev/Xtext/xtend

org.eclipse.xtend.lib.source (2.35.0)

 * License:  EPL-1.0
 * Project: https://eclipse.dev/Xtext/xtend

org.eclipse.xtend.lib (2.33.0)

 * License:  EPL-1.0
 * Project: https://eclipse.dev/Xtext/xtend

org.eclipse.xtend.lib (2.35.0)

 * License:  EPL-1.0
 * Project: https://eclipse.dev/Xtext/xtend

org.eclipse.xtext.builder.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.builder.standalone.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.builder.standalone (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.builder (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.buildship.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.buildship (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types.edit.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types.edit (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types.shared.jdt38.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types.shared.jdt38 (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types.shared.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types.shared (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types.ui.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types.ui (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types (2.33.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.common.types (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ecore.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ecore (2.33.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ecore (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ide.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ide (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.junit4.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.junit4 (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.logging.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.logging (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.m2e.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.m2e (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.smap.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.smap (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.testing.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.testing (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.codemining.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.codemining (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.codetemplates.ide.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.codetemplates.ide (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.codetemplates.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.codetemplates.ui.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.codetemplates.ui (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.codetemplates (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.shared.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.shared (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.testing.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui.testing (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.ui (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.util.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.util (2.33.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.util (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.ide.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.ide (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.junit.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.junit (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.lib.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.lib (2.33.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.lib (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.testing.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.testing (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.ui.source (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase.ui (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase (2.33.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xbase (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext.xtext.generator (2.33.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext (2.33.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.hamcrest.core (2.2.0)

 * License:  BSD-3-Clause

org.junit (4.13.2)

 * License:  EPL-1.0

org.eclipse.cdt_root (11.4.0)

 * License:  EPL-2.0


## Third-party Content (Standalone Distribution)

aopalliance:aopalliance (1.0)

 * License:  LicenseRef-Public-Domain

ch.qos.reload4j:reload4j (1.2.25)

 * License:  Apache-2.0

com.google.code.findbugs:jsr305 (3.0.2)

 * License:  Apache-2.0 and CC-BY-2.5

com.google.code.gson:gson (2.10.1)

 * License:  Apache-2.0

com.google.errorprone:error_prone_annotations (2.26.1)

 * License:  Apache-2.0

com.google.guava:failureaccess (1.0.2)

 * License:  Apache-2.0
 * Project: https://github.com/google/guava

com.google.guava:guava (33.2.0-jre)

 * License:  Apache-2.0 AND CC0-1.0 AND (Apache-2.0 AND CC-PDDC)
 * Project: https://github.com/google/guava

com.google.guava:listenablefuture (9999.0-empty-to-avoid-conflict-with-guava)

 * License:  Apache-2.0
 * Project: https://github.com/google/guava

com.google.inject:guice (7.0.0)

 * License:  Apache-2.0
 * Project: https://github.com/google/guice

io.github.classgraph:classgraph (4.8.172)

 * License:  MIT

jakarta.activation:jakarta.activation-api (2.1.0)

 * License:  EPL-2.0 OR BSD-3-Clause OR GPL-2.0-only with Classpath-exception-2.0

jakarta.annotation:jakarta.annotation-api (2.1.1)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.authentication:jakarta.authentication-api (3.0.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.authorization:jakarta.authorization-api (2.1.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.batch:jakarta.batch-api (2.1.1)

 * License:  Apache-2.0

jakarta.ejb:jakarta.ejb-api (4.0.1)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.el:jakarta.el-api (5.0.1)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.enterprise:jakarta.enterprise.cdi-api (4.0.1)

 * License:  Apache-2.0

jakarta.enterprise:jakarta.enterprise.lang-model (4.0.1)

 * License:  Apache-2.0

jakarta.faces:jakarta.faces-api (4.0.1)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.inject:jakarta.inject-api (2.0.1)

 * License:  Apache-2.0

jakarta.interceptor:jakarta.interceptor-api (2.1.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.jms:jakarta.jms-api (3.1.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.json.bind:jakarta.json.bind-api (3.0.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.json:jakarta.json-api (2.1.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.mail:jakarta.mail-api (2.1.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.persistence:jakarta.persistence-api (3.1.0)

 * License:  EPL-2.0 OR BSD-3-Clause

jakarta.platform:jakarta.jakartaee-api (10.0.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.platform:jakarta.jakartaee-web-api (10.0.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.resource:jakarta.resource-api (2.1.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.security.enterprise:jakarta.security.enterprise-api (3.0.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.servlet.jsp.jstl:jakarta.servlet.jsp.jstl-api (3.0.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.servlet.jsp:jakarta.servlet.jsp-api (3.1.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.servlet:jakarta.servlet-api (6.0.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.transaction:jakarta.transaction-api (2.0.1)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.validation:jakarta.validation-api (3.0.2)

 * License:  Apache-2.0

jakarta.websocket:jakarta.websocket-api (2.1.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.websocket:jakarta.websocket-client-api (2.1.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

jakarta.ws.rs:jakarta.ws.rs-api (3.1.0)

 * License:  EPL-2.0 OR GPL-2.0-only with Classpath-exception-2.0

log4j:log4j (1.2.17)

 * License:  Apache-2.0

org.antlr:antlr-runtime (3.2)

 * License:  BSD-3-Clause

org.checkerframework:checker-qual (3.42.0)

 * License:  MIT

org.commonmark:commonmark-ext-gfm-strikethrough (0.21.0)

 * License:  BSD-2-Clause
 * Project: https://commonmark.org
 * Attribution: Copyright (c) 2015, Robin Stocker

org.commonmark:commonmark-ext-gfm-tables (0.21.0)

 * License:  BSD-2-Clause
 * Project: https://commonmark.org
 * Attribution: Copyright (c) 2015, Robin Stocker

org.commonmark:commonmark-ext-task-list-items (0.21.0)

 * License:  BSD-2-Clause
 * Project: https://commonmark.org
 * Attribution: Copyright (c) 2015, Robin Stocker

org.commonmark:commonmark (0.21.0)

 * License:  BSD-2-Clause
 * Project: https://commonmark.org
 * Attribution: Copyright (c) 2015, Robin Stocker

org.eclipse.elk:org.eclipse.elk.alg.common (0.8.1)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/elk

org.eclipse.elk:org.eclipse.elk.alg.layered (0.8.1)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/elk

org.eclipse.elk:org.eclipse.elk.core (0.9.1)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/elk

org.eclipse.elk:org.eclipse.elk.graph (0.9.1)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/elk

org.eclipse.emf:org.eclipse.emf.common (2.24.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf:org.eclipse.emf.ecore.change (2.14.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf:org.eclipse.emf.ecore.xmi (2.16.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.emf:org.eclipse.emf.ecore (2.26.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/modeling/emf

org.eclipse.lsp4j:org.eclipse.lsp4j.jsonrpc (0.23.1)

 * License:  EPL-2.0 OR BSD-3-Clause
 * Project: https://projects.eclipse.org/projects/technology.lsp4j

org.eclipse.lsp4j:org.eclipse.lsp4j (0.23.1)

 * License:  EPL-2.0 OR BSD-3-Clause
 * Project: https://projects.eclipse.org/projects/technology.lsp4j

org.eclipse.platform:org.eclipse.core.contenttype (3.8.200)

 * License:  EPL-2.0

org.eclipse.platform:org.eclipse.core.expressions (3.8.200)

 * License:  EPL-2.0

org.eclipse.platform:org.eclipse.core.filesystem (1.9.500)

 * License:  EPL-2.0

org.eclipse.platform:org.eclipse.core.jobs (3.13.300)

 * License:  EPL-2.0

org.eclipse.platform:org.eclipse.core.resources (3.18.200)

 * License:  EPL-2.0

org.eclipse.platform:org.eclipse.core.runtime (3.26.100)

 * License:  EPL-2.0

org.eclipse.platform:org.eclipse.equinox.app (1.6.200)

 * License:  EPL-2.0

org.eclipse.platform:org.eclipse.equinox.common (3.17.100)

 * License:  EPL-2.0

org.eclipse.platform:org.eclipse.equinox.preferences (3.10.200)

 * License:  EPL-2.0

org.eclipse.platform:org.eclipse.equinox.registry (3.11.200)

 * License:  EPL-2.0

org.eclipse.platform:org.eclipse.osgi (3.18.300)

 * License:  EPL-2.0

org.eclipse.sprotty:org.eclipse.sprotty.layout (1.1.2)

 * License:  EPL-2.0
 * Project: https://sprotty.org

org.eclipse.sprotty:org.eclipse.sprotty.server (1.1.2)

 * License:  EPL-2.0
 * Project: https://sprotty.org

org.eclipse.sprotty:org.eclipse.sprotty.xtext (1.1.2)

 * License:  EPL-2.0
 * Project: https://sprotty.org

org.eclipse.sprotty:org.eclipse.sprotty (1.1.2)

 * License:  EPL-2.0
 * Project: https://sprotty.org

org.eclipse.xtend:org.eclipse.xtend.lib.macro (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext/xtend

org.eclipse.xtend:org.eclipse.xtend.lib (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext/xtend

org.eclipse.xtext:org.eclipse.xtext.common.types (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext:org.eclipse.xtext.ide (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext:org.eclipse.xtext.util (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext:org.eclipse.xtext.xbase.ide (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext:org.eclipse.xtext.xbase.lib (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext:org.eclipse.xtext.xbase (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext:org.eclipse.xtext (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.eclipse.xtext:xtext-dev-bom (2.35.0)

 * License:  EPL-2.0
 * Project: https://eclipse.dev/Xtext

org.osgi:org.osgi.service.prefs (1.1.2)

 * License:  Apache-2.0

org.osgi:osgi.annotation (8.0.1)

 * License:  Apache-2.0

org.ow2.asm:asm (9.7)

 * License:  BSD-3-Clause


## Third-party Content (Visual Studio Code Extension)

agent-base (6.0.2)

 * License:  MIT

autocompleter (9.3.2)

 * License:  MIT

balanced-match (1.0.2)

 * License:  MIT

brace-expansion (2.0.1)

 * License:  MIT

buffer-crc32 (0.2.13)

 * License:  MIT

debug (4.4.0)

 * License:  MIT

end-of-stream (1.4.4)

 * License:  MIT

extract-zip (2.0.1)

 * License:  BSD-2-Clause
 * Project: https://github.com/max-mapper/extract-zip
 * Attribution: Copyright (c) 2014 Max Ogden and other contributors

fd-slicer (1.1.0)

 * License:  MIT

file-saver (2.0.5)

 * License:  MIT

follow-redirects (1.15.9)

 * License:  MIT

get-stream (5.2.0)

 * License:  MIT

https-proxy-agent (5.0.1)

 * License:  MIT

inherits (2.0.3)

 * License:  ISC

inversify (6.2.0)

 * License:  MIT AND ISC

mime-db (1.52.0)

 * License:  MIT

mime-types (2.1.35)

 * License:  MIT

minimatch (5.1.6)

 * License:  ISC

ms (2.1.3)

 * License:  MIT

nodejs-file-downloader (4.13.0)

 * License:  ISC
 * Project: https://github.com/ibrod83/nodejs-file-downloader

once (1.4.0)

 * License:  ISC

path (0.12.7)

 * License:  MIT

pend (1.2.0)

 * License:  MIT

process (0.11.10)

 * License:  MIT

pump (3.0.2)

 * License:  MIT

reflect-metadata (0.2.2)

 * License:  Apache-2.0

sanitize-filename (1.6.3)

 * License:  ISC OR WTFPL OR (ISC AND WTFPL)

semver (7.6.3)

 * License:  ISC

snabbdom (3.5.1)

 * License:  MIT

sprotty-protocol (1.4.0)

 * License:  EPL-2.0
 * Project: https://sprotty.org

sprotty-protocol (1.4.0)

 * License:  EPL-2.0
 * Project: https://sprotty.org

sprotty-vscode-protocol (1.0.0)

 * License:  EPL-2.0
 * Project: https://sprotty.org

sprotty-vscode-protocol (1.0.0)

 * License:  EPL-2.0
 * Project: https://sprotty.org

sprotty-vscode-webview (1.0.0)

 * License:  EPL-2.0
 * Project: https://sprotty.org

sprotty-vscode (1.0.0)

 * License:  EPL-2.0
 * Project: https://sprotty.org

sprotty (1.4.0)

 * License:  EPL-2.0
 * Project: https://sprotty.org

tinyqueue (2.0.3)

 * License:  ISC

truncate-utf8-bytes (1.0.2)

 * License:  WTFPL

utf8-byte-length (1.0.5)

 * License:  MIT OR WTFPL OR (MIT AND WTFPL)

util (0.10.4)

 * License:  MIT

vscode-jsonrpc (8.2.0)

 * License:  MIT AND LGPL-2.1-or-later

vscode-jsonrpc (8.2.0)

 * License:  MIT AND LGPL-2.1-or-later

vscode-languageclient (9.0.1)

 * License:  MIT

vscode-languageserver-protocol (3.17.5)

 * License:  MIT

vscode-languageserver-protocol (3.17.5)

 * License:  MIT

vscode-languageserver-types (3.17.5)

 * License:  MIT

vscode-languageserver-types (3.17.5)

 * License:  MIT

vscode-messenger-common (0.4.5)

 * License:  MIT

vscode-messenger-common (0.4.5)

 * License:  MIT

vscode-messenger-webview (0.4.5)

 * License:  MIT

vscode-messenger (0.4.5)

 * License:  MIT

vscode-uri (3.0.8)

 * License:  MIT

wrappy (1.0.2)

 * License:  ISC

yauzl (2.10.0)

 * License:  MIT

@inversifyjs/common (1.4.0)

 * License:  MIT

@inversifyjs/core (1.3.5)

 * License:  MIT

@inversifyjs/reflect-metadata-utils (0.2.4)

 * License:  MIT

@types/yauzl (2.10.3)

 * License:  MIT


## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
