/*******************************************************************************
 * Copyright (c) 2012 tieto deutschland gmbh (http://www.tieto.com)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Thomas Jung (initial contribution)
 *    
 *******************************************************************************/ 



package org.eclipse.etrice.tutorials.simulators.trafficlight;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length < 1) {
			System.out
					.println("ERROR: wrong number of arguments - expected >=1 argument");
			printUsage();
			System.exit(1);
		}
		else {
			try {
			    Queue<String> argQueue = new LinkedList<String>(Arrays.asList(args));
				List<Integer> ports = new ArrayList<Integer>();
				Point location = new Point();
				while (null != argQueue.peek()) {
					String arg = argQueue.poll();
					if ("-x".equals(arg)) {
						String coord = argQueue.poll();
						location.x = parseIntFromNextArg(coord);
					} else if ("-y".equals(arg)) {
						String coord = argQueue.poll();
						location.y = parseIntFromNextArg(coord);
					} else {
						Integer val = parseIntFromNextArg(arg);
						ports.add(val);
					}
				}
				int portsArray[] = new int[ports.size()];
				Arrays.setAll(portsArray, ports::get);
				System.out.println("positioning window at (" + location.x + "," + location.y + ")");
				new TrafficLightWnd(portsArray, location);
			}
			catch (Exception e) {
				System.out.println("ERROR: " + e.getMessage());
				printUsage();
				System.exit(1);
			}
		}
	}

	private static Integer parseIntFromNextArg(String arg) throws Exception {
		if (null == arg) {
			throw new Exception("expected an integer but no more arguments left");
		}
		try {
			return Integer.parseInt(arg);
		}
		catch (NumberFormatException e) {
			throw new Exception("integer value expected instead of argument '" + arg + "'");
		}
	}

	public static void printUsage() {
		System.out.println("Usage:   java -jar trafficlight.jar [-x pos] [-y pos] [ip-address]+");
		System.out.println("Example: java -jar trafficlight.jar 4440");
		System.out.println("Example: java -jar trafficlight.jar -x 50 -y 150 4440");
	}

}
