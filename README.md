[![](https://gitlab.eclipse.org/eclipse/etrice/etrice/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.eclipse.org/eclipse/etrice/etrice/-/commits/master)

# eTrice - Real-Time Modeling Tools

* [Home](https://eclipse.dev/etrice)
* [Documentation](https://eclipse.dev/etrice/documentation/release)
* [Downloads](https://eclipse.dev/etrice/downloads/#getting-started)
* [Issues](https://gitlab.eclipse.org/eclipse/etrice/etrice/-/issues)
* [Forum](https://www.eclipse.org/forums/index.php?t=thread&frm_id=199)
* [Developer Wiki](https://gitlab.eclipse.org/groups/eclipse/etrice/-/wikis/home)

## Development

### Eclipse

You can automate the workspace setup with the Oomph setup file [team/ETrice.setup](team/ETrice.setup), see [instructions](https://gitlab.eclipse.org/groups/eclipse/etrice/-/wikis/Development/GettingStartedWithEtrice).

To generate the Xtext languages, please use the Gradle task `generateXtextLanguage` in the respective project to ensure that the correct Xtext version is used.
For example `./gradlew :plugins:org.eclipse.etrice.core.room:generateXtextLanguage` to run the language generator workflow for ROOM.

The language server implementation relies on some external libraries that are not available as Eclipse plugins, e.g. Sprotty.
These can be downloaded and made available to the Eclipse IDE with command `./gradlew installDependencies`.
Please refresh the Eclipse workspace afterwards by pressing 'F5' in the package explorer with all projects selected.

If you want to reproduce the CI build, see [pom.xml](pom.xml).

### Headless Build

The Gradle build requires [JDK 17](https://adoptium.net/de/temurin/releases/?version=17). It can be placed on PATH or configured in the [gradle.properties](gradle.properties) file.

```
./gradlew assemble
```

The Tycho/Maven build for the Eclipse P2 plugins is activated via the Gradle property `-Pmaven`. In addition, it requires JAVA_HOME to be set.

```
export JAVA_HOME="path/to/jdk"
./gradlew assemble -Pmaven
```

### Headless Test

In order to build eTrice C/C++ projects and tests with Gradle, a GCC toolchain must be available on the `PATH`.
For Windows the latest release of MinGW-w64 with UCRT runtime and POSIX threads is recommended, which can be downloaded from [WinLibs](https://winlibs.com/).

```
./gradlew build
```

## Publish

Release builds (commit has version tag) are published to Maven [etrice-releases](https://repo.eclipse.org/content/repositories/etrice-releases) repository and P2 site [download.eclipse.org/etrice](https://download.eclipse.org/etrice). 

Master builds without tag are published to Maven [etrice-snapshots](https://repo.eclipse.org/content/repositories/etrice-snapshots) repository with version `<last version>-SNAPSHOT` and the P2 site [download.eclipse.org/etrice/nightly](https://download.eclipse.org/etrice/nightly). 

All other builds are published to Maven [etrice-snapshots](https://repo.eclipse.org/content/repositories/etrice-snapshots) repository. The version is set to `<last version>-<branch>-SNAPSHOT`, where special chars are replaced with `-`. 
In any case, you can see the current version in the console output via `./gradlew`.
