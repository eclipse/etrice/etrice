/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

import { injectable } from 'inversify';
import { ActionHandlerRegistry, IActionHandler, IActionHandlerInitializer, ICommand, MouseListener, SModelElementImpl } from "sprotty";
import { Action, RequestModelAction, JsonMap } from "sprotty-protocol"
import { SActorRef, SActorClass } from "./model";

export class OpenActorRefMouseListener extends MouseListener {
    override doubleClick(target: SModelElementImpl, event: MouseEvent): (Action | Promise<Action>)[] {
        if (target instanceof SActorRef && event.shiftKey && event.button === 0) {
            return [RequestModelAction.create({
                needsClientLayout: true,
                needsServerLayout: true,
                sourceUri: target.fileURI,
                diagramType: "structure-diagram",
                actorName: target.actorName
            })];
        } else if (target instanceof SActorClass && event.shiftKey && event.button === 0) {
            return [BackAction.create()]
        }
        return [];
    }
}

export interface BackAction extends Action {
    kind: typeof BackAction.KIND
}
export namespace BackAction {
    export const KIND = 'backAction';
    export function create(): BackAction {
        return {
            kind : KIND,
        }
    }
}

/**
 * This Handler implements the ActorRef navigation Feature
 */
@injectable()
export class BackActionHandler implements IActionHandler {

    actionStack: Action[] = [];

    handle(action: Action): void | Action | ICommand {
        if (action.kind === BackAction.KIND) {
            if (this.actionStack.length <= 1) {
                return;
            }
            /*
             * After a RequestAction is returned Sprotty will call the BackActionHandler again with the same RequestAction,
             * collectivly resulting in a single actionStack.pop()
            */
            this.actionStack.pop();
            return this.actionStack.pop();;
        } else if (action.kind === RequestModelAction.KIND) {
            this.actionStack.push(action);
        }
    }
}

@injectable()
export class BackActionHandlerInitializer implements IActionHandlerInitializer {
    initialize(registry: ActionHandlerRegistry): void {
        let handler = new BackActionHandler();
        registry.register(BackAction.KIND, handler);
        registry.register(RequestModelAction.KIND, handler);
    }
    
}