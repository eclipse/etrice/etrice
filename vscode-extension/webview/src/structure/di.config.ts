/*******************************************************************************
 * Copyright (c) 2023 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 		Julian Skublics
 * 
 *******************************************************************************/

import '../../css/structureDiagram.css';
import '../../css/popup.css';
import 'sprotty/css/sprotty.css';

import { Container, ContainerModule } from 'inversify';
import {
    configureModelElement, ConsoleLogger, HtmlRootImpl,
    HtmlRootView, LogLevel, overrideViewerOptions, PreRenderedElementImpl,
    PreRenderedView, SEdgeImpl, SGraphView, SLabelView, SModelRootImpl,
    TYPES, loadDefaultModules, SGraphImpl, SLabelImpl,
    popupFeature, SPortImpl, HiddenBoundsUpdater, moveFeature, nameFeature, openFeature, editFeature
} from 'sprotty';
import { ActorClassView, ActorRefView, BigPortView, BigServicePortView, BindingView, ErrorMessageView, LayeringView, SmallPortView, SmallServicePortView, SubStructureIndicatorView } from './views';
import { SInterfacePort as SInterfacePort, SInternalPort, SActorRef, SActorClass, SErrorMessage } from './model';
import { NoPortResizeHiddenBoundsUpdater } from './no-port-resize';
import { BackActionHandlerInitializer, OpenActorRefMouseListener } from './openActorFeature';

const structureDiagramModule = new ContainerModule((bind, unbind, isBound, rebind) => {
    rebind(TYPES.ILogger).to(ConsoleLogger).inSingletonScope();
    rebind(TYPES.LogLevel).toConstantValue(LogLevel.warn);
    rebind(HiddenBoundsUpdater).to(NoPortResizeHiddenBoundsUpdater).inSingletonScope();
    bind(OpenActorRefMouseListener).toSelf().inSingletonScope();
    bind(TYPES.MouseListener).toService(OpenActorRefMouseListener);
    bind(TYPES.IActionHandlerInitializer).to(BackActionHandlerInitializer);


    const context = { bind, unbind, isBound, rebind };
    configureModelElement(context, 'structure:graph', SGraphImpl, SGraphView, {
        disable: [moveFeature]
    });
    configureModelElement(context, 'label', SLabelImpl, SLabelView, {
        disable: [moveFeature]
    });
    configureModelElement(context, 'error', SErrorMessage, ErrorMessageView, {
        disable: [moveFeature, popupFeature]
    });
    configureModelElement(context, 'structure:actor', SActorClass, ActorClassView, {
        disable: [moveFeature, popupFeature]
    });
    configureModelElement(context, 'structure:interfacePort', SInterfacePort, BigPortView,  {
        enable: [popupFeature],
        disable: [moveFeature]
    });
    configureModelElement(context, 'structure:internalPort', SInternalPort, BigPortView,  {
        enable: [popupFeature],
        disable: [moveFeature]
    });
    configureModelElement(context, 'structure:actorRef', SActorRef, ActorRefView,  {
        enable: [popupFeature,openFeature],
        disable: [moveFeature]
    });
    configureModelElement(context, 'structure:subStructureIndicator', SLabelImpl, SubStructureIndicatorView, {
        disable: [moveFeature]
    })
    configureModelElement(context, 'structure:portRef', SInterfacePort, SmallPortView,  {
        enable: [popupFeature],
        disable: [moveFeature]
    });
    configureModelElement(context, 'structure:bindingEdge', SEdgeImpl, BindingView, {
        enable: [popupFeature],
        disable: [moveFeature, nameFeature, editFeature]
    });
    configureModelElement(context, 'structure:layeringEdge', SEdgeImpl, LayeringView, {
        enable: [popupFeature],
        disable: [moveFeature, nameFeature, editFeature]
    });

    configureModelElement(context, 'structure:spp', SPortImpl, BigServicePortView, {
        enable: [popupFeature],
        disable: [moveFeature]
    });
    configureModelElement(context, 'structure:spp_ref', SPortImpl, SmallServicePortView, {
        enable: [popupFeature],
        disable: [moveFeature]
    });


    // for popups
    configureModelElement(context, 'html', HtmlRootImpl, HtmlRootView);
    configureModelElement(context, 'pre-rendered', PreRenderedElementImpl, PreRenderedView);
    configureModelElement(context, 'palette', SModelRootImpl, HtmlRootView);
});

export function createStructureDiagramContainer(widgetId: string): Container {
    const container = new Container();
    loadDefaultModules(container);
    container.load(structureDiagramModule);
    overrideViewerOptions(container, {
        needsClientLayout: true,
        needsServerLayout: true,
        baseDiv: widgetId,
        hiddenDiv: widgetId + '_hidden'
    });
    return container;
}
