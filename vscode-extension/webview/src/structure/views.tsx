/*******************************************************************************
 * Copyright (c) 2023 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 		Julian Skublics
 * 
 *******************************************************************************/

/** @jsx svg */
import { injectable } from 'inversify';
import { VNode } from 'snabbdom';
import { PolylineEdgeView, RenderingContext, ShapeView, SNodeImpl, SPortImpl, SEdgeImpl, svg, IViewArgs, SLabelImpl } from "sprotty";
import { SInternalPort, SInterfacePort, SErrorMessage } from './model';
import { Point, toDegrees } from 'sprotty-protocol';

@injectable()
export class ErrorMessageView extends ShapeView {
    render(error: Readonly<SErrorMessage>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(error,context)) {
            return undefined;
        }
        return <g>
            <rect class-sprotty-error={true}
                  x="0" y="0" width={Math.max(7 * error.errorMessage.length, 0)} height={Math.max(error.size.height, 0)}>
            </rect>
            <text class-sprotty-error-message={true} x="10" y={error.size.height / 2}>
                {error.errorMessage}
            </text>
                  {context.renderChildren(error)}
        </g>;
    }
}

@injectable()
export class ActorClassView extends ShapeView {
    render(node: Readonly<SNodeImpl>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(node, context)) {
            return undefined;
        }
        return <g>
            <rect class-sprotty-actor-class={true}
                  class-sprotty-actor-class-selected={node.selected}
                  x="0" y="0" width={Math.max(node.size.width, 0)} height={Math.max(node.size.height, 0)}></rect>
            {context.renderChildren(node)}
        </g>;
    }
}

@injectable()
export class ActorRefView extends ShapeView {
    render(node: Readonly<SNodeImpl>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(node, context)) {
            return undefined;
        }
        return <g>
            <rect class-sprotty-actor-ref-selected={node.selected}
                  class-sprotty-actor-ref={true}
                  x="0" y="0" width={Math.max(node.size.width, 0)} height={Math.max(node.size.height, 0)}></rect>
            {context.renderChildren(node)}
        </g>;
    }
}

@injectable()
export class SubStructureIndicatorView extends ShapeView {
    render(label: Readonly<SLabelImpl>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(label,context)) {
            return undefined;
        }
        return <g>
            <rect class-sprotty-subStructure-indicator={true}
                  x="5" y="-2"
                  width="15" height="5">
            </rect>
            <rect class-sprotty-subStructure-indicator={true}
                  x="-1" y="6"
                  width="15" height="5">
            </rect>
            {context.renderChildren(label)}
        </g>
    }
}

@injectable()
export class BigPortView extends ShapeView {
    render(port: Readonly<SInternalPort | SInterfacePort>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(port, context)) {
            return undefined;
        }
        return <g>
            <rect class-sprotty-big-multiplicity-port={true} 
                  class-sprotty-invisible={port.multiplicity <= 1}
                x="4" y="-4" width={Math.max(port.size.width, 0)} height={Math.max(port.size.height, 0)}>
            </rect>
            <rect class-sprotty-big-port-rect={!port.isConjugated}
                  class-sprotty-big-conjugated-port-rect={port.isConjugated}
                  class-sprotty-big-port-selected={port.selected}
                  class-sprotty-big-port-unselected={!port.selected}
                x="0" y="0" width={Math.max(port.size.width, 0)} height={Math.max(port.size.height, 0)}
                >
            </rect>
            <circle class-sprotty-big-port-circle={!port.isConjugated}
                    class-sprotty-big-conjugated-port-circle={port.isConjugated}
                    class-sprotty-invisible={port.isRelayPort}
                cx={Math.max(port.size.width, 0) / 2} cy={Math.max(port.size.height, 0) / 2} r={Math.max(port.size.height, 0) / 4}>
            </circle>
            <rect class-sprotty-big-relay-port-innerRect={!port.isConjugated}
                  class-sprotty-big-conjugated-relay-port-innerRect={port.isConjugated}
                  class-sprotty-invisible={!port.isRelayPort}
                x={Math.max(port.size.width, 0) / 4} y={Math.max(port.size.height, 0) / 4} width={Math.max(port.size.width, 0) * (2/4)} height={Math.max(port.size.height, 0) * (2/4)}>
            </rect>
            {context.renderChildren(port)}
        </g>
    }
}

@injectable()
export class SmallPortView extends ShapeView {
    render(port: Readonly<SInterfacePort>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(port, context)) {
            return undefined;
        }
        return <g>
                <rect class-sprotty-small-multiplicity-port={true} 
                        class-sprotty-invisible={port.multiplicity <= 1}
                    x="2" y="-2" width={Math.max(port.size.width, 0)} height={Math.max(port.size.height, 0)}>
                </rect>
                <rect class-sprotty-small-port={!port.isConjugated}
                      class-sprotty-small-conjugated-port={port.isConjugated}
                      class-sprotty-small-port-selected={port.selected}
                      class-sprotty-small-port-unselected={!port.selected}
                    x="0" y="0" width={Math.max(port.size.width, 0)} height={Math.max(port.size.height, 0)}>
                </rect>
                <rect class-sprotty-small-port={!port.isConjugated}
                      class-sprotty-small-conjugated-port={port.isConjugated}
                    x="0" y="0" width={Math.max(port.size.width, 0)} height={Math.max(port.size.height, 0)}>
                </rect>
                {context.renderChildren(port)}
            </g>;
    }
}


@injectable()
export class BigServicePortView extends ShapeView {
    render(port: Readonly<SPortImpl>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(port, context)) {
            return undefined;
        }
        
        return <g>
            <circle class-sprotty-spp-big={true}
                    class-sprotty-big-port-selected={port.selected}
                    class-sprotty-big-port-unselected={!port.selected}
                cx={Math.max(port.size.width, 0) / 2} cy={Math.max(port.size.height, 0) / 2} r={Math.max(port.size.height, 0) / 2}>
            </circle>
            {context.renderChildren(port)}
        </g>
    }
}

@injectable()
export class SmallServicePortView extends ShapeView {
    render(port: Readonly<SPortImpl>, context: RenderingContext): VNode | undefined {
        if (!this.isVisible(port, context)) {
            return undefined;
        }
        
        return <g>
            <circle class-sprotty-spp-small={true} class-sprotty-small-port-selected={port.selected}
                cx={Math.max(port.size.width, 0) / 2} cy={Math.max(port.size.height, 0) / 2} r={Math.max(port.size.height, 0) / 2}>
            </circle>
            {context.renderChildren(port)}
        </g>
    }
}
@injectable()
export class BindingView extends PolylineEdgeView {
    protected override renderLine(edge: SEdgeImpl, segments: Point[], context: RenderingContext, args?: IViewArgs | undefined): VNode {
        return <g class-sprotty-edge-selected={edge.selected}> {super.renderLine(edge,segments,context,args)}</g>;
    }
}

@injectable()
export class LayeringView extends PolylineEdgeView {

    protected override renderLine(edge: SEdgeImpl, segments: Point[], context: RenderingContext, args?: IViewArgs | undefined): VNode {
        return <g class-sprotty-edge-arrow={true}> {super.renderLine(edge,segments,context,args)}</g>;
    }

    protected override renderAdditionals(edge: SEdgeImpl, segments: Point[], context: RenderingContext): VNode[] {

        const p1 = segments[segments.length - 2]!;
        const p2 = segments[segments.length - 1]!;
        return [
            <g>
            <path class-sprotty-edge-arrow={true} d='M 8,-4 L 0,0'
                transform={`rotate(${this.angle(p2, p1)} ${p2.x} ${p2.y}) translate(${p2.x} ${p2.y})`}/>
            <path class-sprotty-edge-arrow={true} d='M 0,0 L 8,4'
                transform={`rotate(${this.angle(p2, p1)} ${p2.x} ${p2.y}) translate(${p2.x} ${p2.y})`}/>
            </g>
        ];
    }

    angle(x0: Point, x1: Point): number {
        return toDegrees(Math.atan2(x1.y - x0.y, x1.x - x0.x));
    }
}