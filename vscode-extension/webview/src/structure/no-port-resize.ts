/*******************************************************************************
 * Copyright (c) 2023 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 
 *******************************************************************************/

import { BoundsAware, HiddenBoundsUpdater, SModelElementImpl, SPortImpl } from "sprotty";
import { Bounds } from "sprotty-protocol";

export class NoPortResizeHiddenBoundsUpdater extends HiddenBoundsUpdater {
    // workaround to prevent resizing of ports, see https://github.com/eclipse-sprotty/sprotty/issues/165
    protected override getBounds(elm: SVGGraphicsElement, element: SModelElementImpl & BoundsAware): Bounds {
        if (element instanceof SPortImpl && elm.firstChild != null) {
            // Extract the rect element containing the port
            return super.getBounds(elm.firstChild, element);
        }
        return super.getBounds(elm, element);
    }
}
