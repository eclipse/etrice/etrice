import {SNodeImpl, SPortImpl} from "sprotty"

export class SState extends SNodeImpl {
    hasSubGraph: boolean = false;
}

export class STransitionPoint extends SPortImpl {
    isHandler: boolean = false;
}