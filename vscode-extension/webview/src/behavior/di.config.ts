/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

import '../../css/behaviorDiagram.css';
import '../../css/popup.css';
import 'sprotty/css/sprotty.css';

import { Container, ContainerModule } from 'inversify';
import { configureModelElement, ConsoleLogger, editFeature, HtmlRootImpl, HtmlRootView, loadDefaultModules, moveFeature, overrideViewerOptions, popupFeature, PreRenderedElementImpl, PreRenderedView, SEdgeImpl, SGraphImpl, SGraphView, SLabelImpl, SLabelView, SModelRootImpl, SNodeImpl, SPortImpl, TYPES } from 'sprotty';
import { ChoicePointView, EdgeLabelView, EntryPointView, ExitPointView, InitialPointView, PolylineArrowEdgeView, StateMachineView, StateView, SubGraphIndicatorView, TransitionPointView } from './views';
import { SState, STransitionPoint } from './model';
import { SErrorMessage } from '../structure/model';
import { ErrorMessageView } from '../structure/views';


const behaviorDiagramModule = new ContainerModule((bind, unbind, isBound, rebind) => {
    rebind(TYPES.ILogger).to(ConsoleLogger).inSingletonScope();

    const context = { bind, unbind, isBound, rebind };
    configureModelElement(context, 'label', SLabelImpl, SLabelView, {
        disable: [moveFeature]
    });
    configureModelElement(context, 'error', SErrorMessage, ErrorMessageView);
    configureModelElement(context, 'behavior:edgeLabel', SLabelImpl, EdgeLabelView, {
        disable: [moveFeature]
    });
    configureModelElement(context, 'behavior:graph', SGraphImpl, SGraphView, {
        disable: [moveFeature]
    });
    configureModelElement(context, 'behavior:stateMachine', SNodeImpl, StateMachineView, {
        disable: [moveFeature]
    });
    configureModelElement(context, 'behavior:initPoint', SNodeImpl, InitialPointView, {
        disable: [moveFeature, popupFeature]
    });
    configureModelElement(context, 'behavior:state', SState, StateView, {
        disable: [moveFeature]
    });
    configureModelElement(context, 'behavior:subGraphIndicator', SLabelImpl, SubGraphIndicatorView, {
        disable: [moveFeature]
    })
    configureModelElement(context, 'behavior:transition', SEdgeImpl, PolylineArrowEdgeView, {
        disable: [moveFeature, editFeature]
    });
    configureModelElement(context, 'behavior:choicePoint', SNodeImpl, ChoicePointView, {
        disable: [moveFeature]
    });
    configureModelElement(context, 'behavior:transitionPoint', STransitionPoint, TransitionPointView, {
        enable: [popupFeature],
        disable: [moveFeature]
    });
    configureModelElement(context, 'behavior:entryPoint', SPortImpl, EntryPointView, {
        enable: [popupFeature],
        disable: [moveFeature]
    });
    configureModelElement(context, 'behavior:exitPoint', SPortImpl, ExitPointView, {
        enable: [popupFeature],
        disable: [moveFeature]
    });

    // for popups
    configureModelElement(context, 'html', HtmlRootImpl, HtmlRootView);
    configureModelElement(context, 'pre-rendered', PreRenderedElementImpl, PreRenderedView);
    configureModelElement(context, 'palette', SModelRootImpl, HtmlRootView);
});

export function createBehaviorDiagramContainer(widgetId: string): Container {
    const container = new Container();
    loadDefaultModules(container);
    container.load(behaviorDiagramModule);
    overrideViewerOptions(container, {
        needsClientLayout: true,
        needsServerLayout: true,
        baseDiv: widgetId,
        hiddenDiv: widgetId + '_hidden'
    });
    return container;
}