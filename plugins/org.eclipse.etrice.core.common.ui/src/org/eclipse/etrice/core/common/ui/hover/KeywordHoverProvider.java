/*******************************************************************************
 * Copyright (c) 2015 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		https://borisdevnotes.wordpress.com/2014/02/28/xtext-usability-hovers-on-keywords/
 * 		Juergen Haug (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core.common.ui.hover;

import java.io.IOException;
import java.net.URL;
import java.util.Set;

import org.commonmark.node.FencedCodeBlock;
import org.commonmark.node.Node;
import org.commonmark.renderer.NodeRenderer;
import org.commonmark.renderer.html.HtmlNodeRendererContext;
import org.commonmark.renderer.html.HtmlNodeRendererFactory;
import org.commonmark.renderer.html.HtmlWriter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.etrice.core.common.documentation.CommonMarkParser;
import org.eclipse.etrice.core.common.ui.hover.highlight.XtextHtmlHighlighter;
import org.eclipse.etrice.core.common.ui.hover.highlight.XtextTokenScanner;
import org.eclipse.etrice.core.common.ui.internal.CommonActivator;
import org.eclipse.jface.internal.text.html.HTMLPrinter;
import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.Constants;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider;
import org.eclipse.xtext.ui.editor.hover.html.XtextBrowserInformationControlInput;
import org.eclipse.xtext.util.Files;

import com.google.inject.Inject;
import com.google.inject.name.Named;

@SuppressWarnings("restriction")
public class KeywordHoverProvider extends DefaultEObjectHoverProvider {

	public final static String STYLE_SHEET_KEY = "keywordHoverStyleSheetFileName";

	@Inject(optional = true)
	@Named(STYLE_SHEET_KEY)
	private String styleSheetFileName;

	@Inject
	protected AbstractUIPlugin plugin;

	@Inject
	protected ILabelProvider labelProvider;

	@Inject
	protected IKeywordHoverContentProvider contentProvider;

	@Inject
	protected IGrammarAccess grammar;

	@Inject
	@Named(value = Constants.FILE_EXTENSIONS)
	protected String languageKey;	// all associated file extensions, "fileExt1,fileExt2"

	private String styleSheet = null;

	@Override
	protected XtextBrowserInformationControlInput getHoverInfo(EObject element, IRegion hoverRegion,
			XtextBrowserInformationControlInput previous) {
		if (element instanceof Keyword) {
			Keyword keyword = (Keyword) element;
			String html = contentProvider.getHTMLContent(keyword.getValue());
			if (html == null) {
				String md = contentProvider.getMdContent(keyword.getValue());
				if(md != null) {
					html = CommonMarkParser.toHtml(md, new HtmlNodeRendererFactory() {
						public NodeRenderer create(HtmlNodeRendererContext context) {
							return new HighlightedCodeBlockNodeRenderer(context);
						}
					});
				}
			}
			if (html != null) {
				// copied from XbaseHoverProvider
				StringBuilder builder = new StringBuilder(html);
				ColorRegistry registry = JFaceResources.getColorRegistry();
				RGB fgRGB = registry.getRGB("org.eclipse.ui.workbench.HOVER_FOREGROUND"); //$NON-NLS-1$
				RGB bgRGB = registry.getRGB("org.eclipse.ui.workbench.HOVER_BACKGROUND"); //$NON-NLS-1$
				// adjust colors to have it readable in dark mode, see room.ui css
				String codeBGColor = (fgRGB != null && fgRGB.getHSB()[2] >= 0.5) ? "#505050" : "#dcdcdc";
				String keywordCSS = getKeywordStyleSheet().replace("CODE_BG_COLOR", codeBGColor);
				if (fgRGB != null && bgRGB != null) {
					HTMLPrinter.insertPageProlog(builder, 0, fgRGB, bgRGB, keywordCSS);
				} else {
					HTMLPrinter.insertPageProlog(builder, 0, keywordCSS);
				}
				HTMLPrinter.addPageEpilog(builder);
				return new XtextBrowserInformationControlInput(previous, element, builder.toString(), labelProvider);
			}
		}
		return super.getHoverInfo(element, hoverRegion, previous);

	}

	protected String getKeywordStyleSheet() {
		String superStyle = super.getStyleSheet();
		if (styleSheet == null) {
			styleSheet = "";
			try {
				URL url = null;
				if (plugin != null && styleSheetFileName != null) {
					url = plugin.getBundle().getEntry(styleSheetFileName);
				} else {
					url = CommonActivator.getInstance().getBundle().getEntry("/eTriceKeywordHoverStyle.css");
				}
				styleSheet = Files.readStreamIntoString(url.openStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return superStyle + styleSheet;
	}

	class HighlightedCodeBlockNodeRenderer implements NodeRenderer {

		private final HtmlWriter html;
		private final XtextHtmlHighlighter highlighter;

		HighlightedCodeBlockNodeRenderer(HtmlNodeRendererContext context) {
			this.html = context.getWriter();
			Set<String> keywords = GrammarUtil.getAllKeywords(grammar.getGrammar());
			this.highlighter = new XtextHtmlHighlighter(
					new XtextTokenScanner(languageKey.replace(',', '-'), keywords.toArray(new String[0])));
		}

		@Override
		public Set<Class<? extends Node>> getNodeTypes() {
			return Set.of(FencedCodeBlock.class);
		}

		@Override
		public void render(Node node) {
			FencedCodeBlock codeBlock = (FencedCodeBlock) node;
			html.line();
			if (languageKey.contains(codeBlock.getInfo())) {
				html.raw(highlighter.htmlStartTags());
				html.raw(highlighter.highlight(codeBlock.getLiteral()));
				html.raw(highlighter.htmlEndTags());
			} else {
				html.tag("pre");
				html.tag("code");
				html.text(codeBlock.getLiteral());
				html.tag("/code");
				html.tag("/pre");
			}
			html.line();
		}
	}
}
