/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

 
package org.eclipse.etrice.ls.diagram;

import java.util.function.Consumer;

import org.eclipse.sprotty.SPort;

/**
 * Sprotty representation of an eTrice transition point.
 */
public class STransitionPoint extends SPort {

	private boolean isHandler;

	public STransitionPoint(final Consumer<STransitionPoint> initializer) {
		initializer.accept(this);
	}

	public void setIsHandler(boolean isHandler) {
		this.isHandler = isHandler;
	}

	public boolean isHandler() {
		return isHandler;
	}
	
}
