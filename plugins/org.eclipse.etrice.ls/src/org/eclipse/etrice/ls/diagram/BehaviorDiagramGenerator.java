/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.diagram;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.etrice.core.fsm.fSM.ChoicePoint;
import org.eclipse.etrice.core.fsm.fSM.ChoicepointTerminal;
import org.eclipse.etrice.core.fsm.fSM.EntryPoint;
import org.eclipse.etrice.core.fsm.fSM.ExitPoint;
import org.eclipse.etrice.core.fsm.fSM.InitialTransition;
import org.eclipse.etrice.core.fsm.fSM.NonInitialTransition;
import org.eclipse.etrice.core.fsm.fSM.State;
import org.eclipse.etrice.core.fsm.fSM.StateGraph;
import org.eclipse.etrice.core.fsm.fSM.StateGraphItem;
import org.eclipse.etrice.core.fsm.fSM.StateTerminal;
import org.eclipse.etrice.core.fsm.fSM.SubStateTrPointTerminal;
import org.eclipse.etrice.core.fsm.fSM.TrPoint;
import org.eclipse.etrice.core.fsm.fSM.TrPointTerminal;
import org.eclipse.etrice.core.fsm.fSM.Transition;
import org.eclipse.etrice.core.fsm.fSM.TransitionPoint;
import org.eclipse.etrice.core.fsm.fSM.TransitionTerminal;
import org.eclipse.etrice.core.fsm.naming.FSMNameProvider;
import org.eclipse.etrice.core.room.ActorClass;
import org.eclipse.sprotty.Dimension;
import org.eclipse.sprotty.SEdge;
import org.eclipse.sprotty.SLabel;
import org.eclipse.sprotty.SModelElement;
import org.eclipse.sprotty.SNode;
import org.eclipse.sprotty.SPort;
import org.eclipse.sprotty.xtext.IDiagramGenerator.Context;
import org.eclipse.sprotty.xtext.SIssueMarkerDecorator;
import org.eclipse.sprotty.xtext.tracing.ITraceProvider;

public class BehaviorDiagramGenerator {

	private static final FSMNameProvider fsmNameProvider = new FSMNameProvider();
	private ITraceProvider traceProvider;
	private SIssueMarkerDecorator issueMarker;

	public static final double INNER_TRANSITIONPOINT_SIZE = 10d;
	public static final double OUTER_TRANSITIONPOINT_SIZE = 20d;
	public static final double CHOICEPOINT_SIZE = 20d;
	public static final double INIT_POINT_SIZE = 20d;

	public static final String EDGE_LABEL = "behavior:edgeLabel";
	public static final String STATE_MACHINE_TYPE = "behavior:stateMachine";
	public static final String INIT_POINT_TYPE = "behavior:initPoint";
	public static final String STATE_TYPE = "behavior:state";
	public static final String SUBGRAPH_INDICATOR = "behavior:subGraphIndicator";
	public static final String TRANSITION_TYPE = "behavior:transition";
	public static final String CHOICEPOINT_TYPE = "behavior:choicePoint";
	public static final String TRANSITIONPOINT_TYPE = "behavior:transitionPoint";
	public static final String ENTRYPOINT_TYPE = "behavior:entryPoint";
	public static final String EXITPOINT_TYPE = "behavior:exitPoint";

	public BehaviorDiagramGenerator(ITraceProvider traceProvider, SIssueMarkerDecorator issueMarker) {
		this.traceProvider = traceProvider;
		this.issueMarker = issueMarker;
	}

	public SNode generateBehaviorDiagram(ActorClass ac, Context context) {
		// TODO hierarchical state machines and inheritance
		
		if (ac.getStateMachine() == null) {
			return new SErrorMessage(error -> {
				error.setSize(new Dimension(DiagramGenerator.ERROR_NODE_SIZE_WIDTH, DiagramGenerator.ERROR_NODE_SIZE_HEIGHT));
				error.setErrorMessage("Could not generate Diagram: This Actor does not have a StateMachine!");
			});	
		}
		StateGraph stateGraph = ac.getStateMachine();
		SNode initialPoint = new SNode(node -> {
			node.setId(uniqueId(context, "#initialPoint"));
			node.setSize(new Dimension(INIT_POINT_SIZE, INIT_POINT_SIZE));
			node.setType(INIT_POINT_TYPE);
		});
		Stream<SState> states = stateGraph.getStates().stream().map(state -> {
			SLabel stateName = new SLabel(label -> {
				label.setText(state.getName());
				label.setId(uniqueId(context, toId(state) + "#label"));
			});
			return new SState(node -> {
				if (state.getSubgraph() != null) {
					Stream<SPort> entryAndExitPoints = state.getSubgraph().getTrPoints().stream()
						.filter(trPoint -> trPoint instanceof ExitPoint || trPoint instanceof EntryPoint).map(trPoint -> {
							SLabel trPointName = new SLabel(label -> {
								label.setText(trPoint.getName());
								label.setId(uniqueId(context, toId(trPoint) + "#label"));
							});
							return new SPort(port -> {
								port.setChildren(List.of(trPointName));
								port.setId(uniqueId(context, toId(trPoint, toId(state))));
								if (trPoint instanceof EntryPoint) {
									port.setType(ENTRYPOINT_TYPE);
								} else {
									port.setType(EXITPOINT_TYPE);
								}
								port.setSize(new Dimension(INNER_TRANSITIONPOINT_SIZE, INNER_TRANSITIONPOINT_SIZE));
								traceAndMark(port, trPoint, context);
							});
						}
					);
					SLabel subGraphIndicator = new SLabel(indicator -> {
						indicator.setType(SUBGRAPH_INDICATOR);
						indicator.setId(uniqueId(context, toId(state) + "#subGraph"));
						//This Text is not rendered in sprotty, but the layout engine needs a placeholder to compute the position of the label
						indicator.setText("placeholder");
					});
					node.setChildren(Stream.of(entryAndExitPoints, Stream.of(stateName), Stream.of(subGraphIndicator)).<SModelElement>flatMap(Function.identity()).collect(Collectors.toList()));				} else {
					node.setChildren(List.of(stateName));
				}
				node.setType(STATE_TYPE);
				node.setId(uniqueId(context, toId(state)));
				node.setHasSubGraph(state.getSubgraph() != null);
				traceAndMark(node, state, context);
			});
		});
		Stream<SEdge> transitions = stateGraph.getTransitions().stream().map(transition -> {
			SLabel transitionName = new SLabel(label -> {
				label.setType(EDGE_LABEL);
				label.setText(fsmNameProvider.getTransitionLabelName(transition));
				label.setId(uniqueId(context, toId(transition) + "#label"));
			});
		
			return new SEdge(edge -> {
				edge.setChildren(List.of(transitionName));
				edge.setId(uniqueId(context, toId(transition)));
				edge.setSourceId(sourceId(transition));
				edge.setTargetId(targetId(transition));
				edge.setType(TRANSITION_TYPE);
				traceAndMark(edge, transition, context);
			});
		});
		Stream<SNode> choicePoints = stateGraph.getChPoints().stream().map(ch -> {
			return new SNode(node -> {
				node.setId(uniqueId(context, toId(ch)));
				node.setType(CHOICEPOINT_TYPE);
				node.setSize(new Dimension(CHOICEPOINT_SIZE, CHOICEPOINT_SIZE));
				traceAndMark(node, ch, context);
			});
		});
		Stream<SPort> transitionPoints = stateGraph.getTrPoints().stream()
			.filter(TransitionPoint.class::isInstance).map(TransitionPoint.class::cast).map(trPoint -> {
				SLabel trPointName = new SLabel(label -> {
					label.setText(trPoint.getName());
					label.setId(uniqueId(context, toId(trPoint) + "#label"));
				});
				return new STransitionPoint(port -> {
					port.setIsHandler(trPoint.isHandler());
					port.setChildren(List.of(trPointName));
					port.setId(uniqueId(context, toId(trPoint)));
					port.setType(TRANSITIONPOINT_TYPE);
					port.setSize(new Dimension(OUTER_TRANSITIONPOINT_SIZE, OUTER_TRANSITIONPOINT_SIZE));
					traceAndMark(port, trPoint, context);
				}
			);
		});

		SLabel stateMachineLabel = new SLabel(label -> {
			label.setText(ac.getName() + ": StateMachine");
		});

		List<SModelElement> children = Stream.of(Stream.of(stateMachineLabel), transitionPoints, states, choicePoints, transitions, Stream.of(initialPoint))
			.<SModelElement>flatMap(Function.identity()).collect(Collectors.toList());
		return new SNode(node -> {
			node.setType(STATE_MACHINE_TYPE);
			node.setChildren(children);
			node.setId(uniqueId(context, "#statemachine"));
		});
	}

	private String toId(State state) {
		return state.getName();
	}
	private String toId(ChoicePoint choicePoint) {
		return choicePoint.getName();
	}
	private String toId(TrPoint trPoint, String parentId) {
		return parentId + "." + toId(trPoint);
	}
	private String toId(TrPoint trPoint) {
		return trPoint.getName();
	}
	private String toId(Transition transition) {
		return fsmNameProvider.getTransitionLabelName(transition) + ":" + sourceId(transition) + "->" + targetId(transition);
	}
	private String toId(TransitionTerminal transitionTerminal) {
		if (transitionTerminal instanceof StateTerminal) {
			return toId(((StateTerminal) transitionTerminal).getState());
		} else if (transitionTerminal instanceof ChoicepointTerminal){
			return toId(((ChoicepointTerminal) transitionTerminal).getCp());
		} else if (transitionTerminal instanceof TrPointTerminal) {
			return toId(((TrPointTerminal)transitionTerminal).getTrPoint());
		} else if (transitionTerminal instanceof SubStateTrPointTerminal){
			return toId(((SubStateTrPointTerminal)transitionTerminal).getState()) + "." + toId(((SubStateTrPointTerminal)transitionTerminal).getTrPoint());
		} else {
			throw new RuntimeException("Unreachable");
		}
	}
	private String sourceId(Transition transition) {
		if (transition instanceof InitialTransition) {
			return "#initialPoint";
		} else {
			return toId(((NonInitialTransition) transition).getFrom());
		}
	}
	private String targetId(Transition transition) {
		return toId(transition.getTo());
	}

	private void traceAndMark(SModelElement sElement, StateGraphItem item, Context context) {
		traceProvider.trace(sElement, item);
		issueMarker.addIssueMarkers(sElement, item, context);
	}

	/*
	 * If two SModelElements have the same id, the DiagramServer will crash in the layout process
	 * To prevent a DiagramServer Crash if to eTrice Elements have the same name a number is added if two id's are identical
	 */
	private String uniqueId(Context context, String idProposal) {
		return context.getIdCache().uniqueId(idProposal);
	}
}
