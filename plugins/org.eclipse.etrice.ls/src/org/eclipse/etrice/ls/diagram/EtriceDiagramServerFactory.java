/*******************************************************************************
 * Copyright (c) 2023 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.diagram;

import java.util.List;

import org.eclipse.sprotty.xtext.DiagramServerFactory;

public class EtriceDiagramServerFactory extends DiagramServerFactory {
	public static final String STRUCTURE_DIAGRAM_TYPE = "structure-diagram";
	public static final String BEHAVIOR_DIAGRAM_TYPE = "behavior-diagram";
	
	@Override
	public List<String> getDiagramTypes() {
		return List.of(STRUCTURE_DIAGRAM_TYPE, BEHAVIOR_DIAGRAM_TYPE);
	}
	
}
