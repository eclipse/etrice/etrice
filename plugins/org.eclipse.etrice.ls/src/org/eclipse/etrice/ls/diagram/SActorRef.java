/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.diagram;

import java.util.function.Consumer;
import org.eclipse.sprotty.SNode;

/**
 * Sprotty representation of an eTrice ActorReference
 */
public class SActorRef extends SNode {

	private String fileURI;
	private String actorName;
	
	public SActorRef() {
		this.setType(StructureDiagramGenerator.ACTOR_REF_TYPE);
	}
	public SActorRef(final Consumer<SActorRef> initializer) {
	    this();
	    initializer.accept(this);
	}
	public void setFileUri(String fileURI) {
		this.fileURI = fileURI;
	}
	public void setActorName(String actorName) {
		this.actorName = actorName;
	}
	public String getFileUri() {
		return fileURI;
	}
	public String getActorName() {
		return actorName;
	}
}
