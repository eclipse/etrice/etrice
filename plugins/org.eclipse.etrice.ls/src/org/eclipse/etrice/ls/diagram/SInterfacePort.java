/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.diagram;

import java.util.function.Consumer;
import org.eclipse.sprotty.SPort;

/**
 * Sprotty representation of an interface eTrice Port
 */
public class SInterfacePort extends SPort {
	private boolean isConjugated;
	private boolean isRelayPort;
	private int multiplicity;
	
	public SInterfacePort() {
		this.setType(StructureDiagramGenerator.INTERFACE_PORT_TYPE);
	}
	public SInterfacePort(final Consumer<SInterfacePort> initializer) {
	    this();
	    initializer.accept(this);
	}
	
	
	public boolean isConjugated() {
		return isConjugated;
	}
	public void setIsConjugated(boolean isConjugated) {
		this.isConjugated = isConjugated;
	}
	public int getMultiplicity() {
		return multiplicity;
	}
	public void setMultiplicity(int multiplicity) {
		this.multiplicity = multiplicity;
	}
	public boolean isRelayPort() {
		return isRelayPort;
	}
	public void setIsRelayPort(boolean isRelayPort) {
		this.isRelayPort = isRelayPort;
	}
}
