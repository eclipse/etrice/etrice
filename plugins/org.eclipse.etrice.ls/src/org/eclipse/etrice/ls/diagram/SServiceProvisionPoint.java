/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ls.diagram;

import java.util.function.Consumer;

import org.eclipse.sprotty.SPort;

/**
 * Sprotty representation of a ServiceProvisonPoint
 */
public class SServiceProvisionPoint extends SPort {
	public SServiceProvisionPoint() {
		this.setType(StructureDiagramGenerator.SPP_EXTERNAL_TYPE);
	}
	public SServiceProvisionPoint(Consumer<SServiceProvisionPoint> initializer) {
		this();
		initializer.accept(this);
	}
}
