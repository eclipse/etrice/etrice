package org.eclipse.etrice.core.common.ide;

import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.lsp4j.WorkspaceSymbol;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.xtext.findReferences.IReferenceFinder.IResourceAccess;
import org.eclipse.xtext.ide.server.symbol.DocumentSymbolMapper;
import org.eclipse.xtext.ide.server.symbol.DocumentSymbolService;
import org.eclipse.xtext.ide.server.symbol.HierarchicalDocumentSymbolService;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

import com.google.inject.Inject;

/**
 * The {@link DocumentSymbolService} provides old style document symbols (pre LSP 3.10.0), but it also provides the workspace symbols for newer lsp versions.
 * The new hierarchical document symbols are provided by the {@link HierarchicalDocumentSymbolService} instead which uses the {@link DocumentSymbolMapper}.
 * 
 * We overwrite the workspace symbol generation to use the same providers from {@link DocumentSymbolMapper} that are used for document symbols.
 */
public class PatchedDocumentSymbolService extends DocumentSymbolService  {
	
	/**
	 * Provides the {@link WorkspaceSymbol#getContainerName() container name} for the document symbol.
	 * 
	 * @see WorkspaceSymbol#getContainerName()
	 */
	public static class WorkspaceSymbolContainerNameProvider {
		public String getContainerName(IEObjectDescription description) {
			QualifiedName qualifiedName = description.getQualifiedName();
			if(qualifiedName == null || qualifiedName.isEmpty()) { return null; }
			return qualifiedName.skipLast(1).toString();
		}
	}
	
	@Inject
	private DocumentSymbolMapper.DocumentSymbolNameProvider nameProvider;
	
	@Inject
	private DocumentSymbolMapper.DocumentSymbolKindProvider kindProvider;
	
	@Inject
	private WorkspaceSymbolContainerNameProvider containerNameProvider;
	
	@Override
	protected void createSymbol(IEObjectDescription description, IResourceAccess resourceAccess, Procedure1<? super WorkspaceSymbol> acceptor) {
		String name = nameProvider.getName(description);
		if (name == null) { return; }
		SymbolKind kind = kindProvider.getSymbolKind(description);
		if (kind == null) {	return; }
		String containerName = containerNameProvider.getContainerName(description);
		getSymbolLocation(description, resourceAccess, (Location location) -> {
			WorkspaceSymbol symbol = new WorkspaceSymbol(name, kind, Either.forLeft(location), containerName);
			acceptor.apply(symbol);
		});
	}
}
