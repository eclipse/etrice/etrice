/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Julian Skublics (initial contribution)
 * 
 *******************************************************************************/
package org.eclipse.etrice.core.ide;

import com.google.inject.Module;


/**
 * Service Provider Interface
 * for the DiagramService defined in 
 * org.eclipse.etrice.ls/META-INF/services/org.eclipse.etrice.core.ide.IRoomDiagramModule
 */
public interface IRoomDiagramModule extends Module {}
