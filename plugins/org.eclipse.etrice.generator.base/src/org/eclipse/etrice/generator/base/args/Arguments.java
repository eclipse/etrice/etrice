/*******************************************************************************
* Copyright (c) 2018 protos software gmbh (http://www.protos.de).
* All rights reserved.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*
* CONTRIBUTORS:
*           Jan Belle (initial contribution)
*
 *******************************************************************************/

package org.eclipse.etrice.generator.base.args;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.StringJoiner;

/**
 * Encapsulates a set of arguments.
 */
public class Arguments {
	
	private Options options;
	private LinkedHashMap<String, Object> option2Arg;
	
	/**
	 * Creates a new set of arguments for a set of options.
	 * 
	 * @param options the options to create the arguments from
	 */
	public Arguments(Options options) {
		this.options = options;
		option2Arg = new LinkedHashMap<>();
	}
	
	/**
	 * @param name the name of the option
	 * @return whether an argument is set for the option of this name
	 */
	public boolean hasArgument(String name) {
		return hasArgument(options.get(name));
	}
	
	/**
	 * @param option the option of the argument
	 * @return whether an argument is set for the option
	 */
	public <T> boolean hasArgument(Option<T> option) {
		return option2Arg.containsKey(option.getName());
	}
	
	/**
	 * Queries an argument by its name.
	 * If no argument is set for the option, the default value of that option is returned.
	 * 
	 * @param name the name of the option
	 * @return the argument
	 */
	public Object get(String name) throws IllegalArgumentException {
		return get(options.get(name));
	}
	
	/**
	 * Queries an argument.
	 * If no argument is set for the option, the default value of that option is returned.
	 * 
	 * @param option the option of the argument
	 * @return the argument
	 */
	public <T> T get(Option<T> option) throws IllegalArgumentException {
		Object value = option2Arg.getOrDefault(option.getName(), option.getDefaultValue());
		return option.getType().cast(value);
	}
	
	/**
	 * Sets an argument to the passed value.
	 * 
	 * @param name the name of the option
	 * @param value the value to be set
	 */
	public void set(String name, Object value) throws IllegalArgumentException {
		Option<?> opt = options.get(name);
		if(!opt.getType().isInstance(value)) {
			throw new IllegalArgumentException("value " + value.toString() + " is not assignable to Option " + opt.toString());
		}
		option2Arg.put(name, value);
	}
	
	/**
	 * Sets an argument to the passed value.
	 * 
	 * @param option the option
	 * @param value the value to be set
	 */
	public <T> void set(Option<T> option, T value) throws IllegalArgumentException {
		option2Arg.put(option.getName(), value);
	}
	
	/**
	 * Returns the options for the arguments.
	 * 
	 * @return options the options
	 */
	public Options getOptions() {
		return options;
	}
	
	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(", ");
		for(Entry<String, Object> entry: option2Arg.entrySet()) {
			joiner.add(argToString(entry.getKey(), entry.getValue()));
		}
		return joiner.toString();
	}
	
	private String argToString(String name, Object value) {
		String result = name + "=";
		if(value.getClass().isArray()) {
			Object[] arr = (Object[]) value;
			result += Arrays.toString(arr);
		}
		else {
			result += value.toString();
		}
		return result;
	}
}
