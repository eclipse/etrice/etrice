/*******************************************************************************
* Copyright (c) 2018 protos software gmbh (http://www.protos.de).
* All rights reserved.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*
* CONTRIBUTORS:
*           Jan Belle (initial contribution)
*
 *******************************************************************************/

package org.eclipse.etrice.generator.base.cli;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Stream;

import org.eclipse.etrice.generator.base.args.Arguments;
import org.eclipse.etrice.generator.base.args.BooleanOption;
import org.eclipse.etrice.generator.base.args.EnumOption;
import org.eclipse.etrice.generator.base.args.Option;
import org.eclipse.etrice.generator.base.args.Options;
import org.eclipse.etrice.generator.base.args.PathOption;
import org.eclipse.etrice.generator.base.args.StringOption;

/**
 * Simple implementation of a command line parser.
 * Options must start with a prefixed single dash and parameters must be separated with a space.
 * Everything remaining is interpreted as input files.
 * <br>
 * Example:
 *  -genDir myGenDir MyFile.txt
 * 
 */
public class CommandLineParser implements ICommandLineParser {
	
	public CommandLineParser() {
	}
	
	@Override
	public Arguments parseArgs(Options options, Option<String[]> defaultOption, List<String> args) throws CommandLineParseException {
		Arguments parsedArgs = new Arguments(options);
		List<String> nArgs = normalize(args);
		ListIterator<String> iterator = nArgs.listIterator();
		LinkedList<String> defaultArgs = new LinkedList<>();
		
		while(iterator.hasNext()) {
			String str = iterator.next().trim();
			if(str.startsWith("-")) {
				Option<?> opt = parseOption(options, str);
				parseValue(opt, iterator, parsedArgs);
			}
			else {
				defaultArgs.add(str);
			}
		}
		
		if(!defaultArgs.isEmpty()) {
			parsedArgs.set(defaultOption, defaultArgs.toArray(new String[0]));
		}
		
		return parsedArgs;
	}
	
	private List<String> normalize(List<String> args) throws CommandLineParseException {
		List<String> nArgs = new LinkedList<>(args);
		return nArgs;
	}
	
	private Option<?> parseOption(Options options, String str) throws CommandLineParseException {
		try {
			String optionName = str.substring(1);
			Option<?> opt = options.get(optionName);
			return opt;
		}
		catch(IllegalArgumentException e) {
			throw new CommandLineParseException("Option " + str + " not recognized");
		}
	}
	
	private void parseValue(Option<?> opt, ListIterator<String> iterator, Arguments parsedArgs) throws CommandLineParseException {
		if(opt instanceof BooleanOption boolOpt) {
			// boolean options don't have a value on the command line
			parsedArgs.set(boolOpt, true);
		}
		else {
			if(!iterator.hasNext()) {
				throw new CommandLineParseException("Expected one argument for option " + opt.getName());
			}
			
			String str = iterator.next();
			if(opt instanceof StringOption strOpt) {
				parsedArgs.set(strOpt, str);
			}
			else if(opt instanceof PathOption pathOpt) {
				// combine the paths if the option appears more than once in the command line arguments
				String[] currentPath = parsedArgs.hasArgument(pathOpt) ? parsedArgs.get(pathOpt) : new String[] {};
				String[] value = str.split(File.pathSeparator);
				String[] combinedPath = Stream.concat(Arrays.stream(currentPath), Arrays.stream(value)).toArray(String[]::new);
				parsedArgs.set(pathOpt, combinedPath);
			}
			else if(opt instanceof EnumOption<?> enumOpt) {
				Enum<?> value = parseEnum(enumOpt, str);
				parsedArgs.set(enumOpt.getName(), value);
			}
			else {
				throw new CommandLineParseException("Option " + opt.getName() + " is not supported on the command line");
			}
		}
	}
	
	private Enum<?> parseEnum(EnumOption<?> opt, String str) {
		Enum<?>[] constants = opt.getType().getEnumConstants();
		
		for(Enum<?> c: constants) {
			if(c.toString().equalsIgnoreCase(str)) {
				return c;
			}
		}
		
		throw new CommandLineParseException("Argument " + str + " not allowed for option " + opt.getName());
	}
}
