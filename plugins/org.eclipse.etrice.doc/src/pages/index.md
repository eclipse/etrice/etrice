---
title: Eclipse eTrice - Real-Time Modeling Tools
hide_table_of_contents: true
---

<img src="img/etrice-logo-cmyk.svg" alt="eTrice logo" style="height: 78px"/>

# Eclipse eTrice - Real-Time Modeling Tools

eTrice provides an implementation of the ROOM (Real-Time Object-Oriented
Modeling) modeling language together with editors, code generators for
Java, C++ and C code and target middleware.  
The model is defined in textual form (Xtext) with graphical editors
(Graphiti) for the structural and behavioral (i.e. state machine) parts.

**eTrice is all about the reduction of complexity:**

- **structural complexity**
  - by explicit modeling of hierarchical Actor containment and layering
- **behavioral complexity**
  - by hierarchical state machines
- **teamwork complexity**
  - because loosely coupled Actors provide a natural way to structure
    team work
  - since textual model notation allows simple branching and merging
- **complexity of concurrent & distributed systems**
  - because loosely coupled Actors are deployable to threads, processes,
    nodes
- **complexity of variant handling and reuse (e.g. for product lines)**
  - by composition of existing Actors to new structures
  - since Protocols and Ports make Actors replaceable
  - by inheritance for structure, behavior and Protocols
  - by making use of model level libraries
- **complexity of debugging**
  - model level debugging: state machine animation, data inspection and
    manipulation, message injection, generated message sequence charts
  - model checking easier for model than for code (detect errors before
    they occur)

### Some Highlights of eTrice

#### Textual Model Editor

The model files are stored in a purely textual and easily readable form.
The DSL is created using Xtext and a fully featured editor with syntax
highlighting, content assist, template proposals and outline view is
provided.

The models allow to import other model parts and thus allow an arbitrary
granularity as well as working with modeling libraries. Another great
advantage is the relatively simple and understandable behavior in the
case of merge conflicts. The user will be able to resolve conflicts by
reviewing the changes.

![textual ROOM model editor](/img/text_editor.jpg)

#### Graphical Structure Editor

The Structure Editor allows to edit the Actor Structure in a convenient
way. It is possible to create and arrange actor references and ports and
to create bindings and layer connections.

![graphical ROOM structure editor](/img/structure_editor.jpg)

#### Graphical Behavior Editor

The Behavior Editor allows to describe the Actor Behavior by means of a
hierarchical finite state machine. It is possible to create and arrange
states, choice points, transition points and transitions.

![graphical ROOM behavior editor](/img/behavior_editor.jpg)

#### Code Generators

Currently eTrice has two generators for ANSI-C and for Java code. A C++
generator is currently being developed. We strive to achieve a good
readability of the generated code.

#### Java and C Runtime

The Runtimes provide basic infrastructure like messaging and debug
support in form of the generation of sequence diagrams from the running
application. Thanks to the good Java (JDT) and C (CDT) integration
within the Eclipse IDE very short round trip times can be achieved.
