---
title: eTrice Support
hide_table_of_contents: true
---

# Support

* [GitLab Issues](https://gitlab.eclipse.org/eclipse/etrice/etrice/-/issues)
* [Eclipse Community Forums](https://www.eclipse.org/forums/index.php?t=thread&frm_id=199)
* [Professional Support by Protos](https://www.protos.de/produkte/modellierung-und-codegenerierung-fuer-embedded-systeme-mit-etrice/)