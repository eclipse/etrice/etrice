import React, {useEffect, useState} from 'react';
import CodeBlock from '@theme/CodeBlock';
import axios from 'axios';

export function LatestReleaseVersion() {
  const [releaseVersion, setReleaseVersion] = useState("x.x.x");
  useEffect(() => {
    requestLatestVersion(
      (version) => {
        setReleaseVersion(version);
      },
      () => {
        setReleaseVersion('error');
      }
    );
  }, [releaseVersion]);
  return <>{releaseVersion}</>;
}

export function LatestReleaseUpdateSite() {
  const [releaseVersion, setReleaseVersion] = useState("x.x.x");
  useEffect(() => {
    requestLatestVersion(
      (version) => {
        setReleaseVersion(version);
      },
      () => {
        setReleaseVersion('error');
      }
    );
  }, [releaseVersion]);
  return <CodeBlock className='latest-update-site'>https://download.eclipse.org/etrice/{releaseVersion}/updates</CodeBlock>;
}

function requestLatestVersion(successCb, errorCb) {
  axios.get('https://gitlab.eclipse.org/api/v4/projects/1863/releases')
    .then(function (response) {
      console.log(response.data)
      const lastRelease = response.data.find(d => !d.upcoming_release && d.released_at && d.tag_name);
      if(lastRelease) {
        const version = lastRelease.tag_name.replace('v_', '');
        successCb(version);
      }
      else {
        errorCb();
      }
    })
    .catch(function (error) {
      console.log(error);
      errorCb();
    })
    .finally(function () {
      // nop
    });
}
