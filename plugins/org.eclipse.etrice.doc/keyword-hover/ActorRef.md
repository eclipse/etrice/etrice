An ActorRef is an instance of an ActorClass

```room
/** Documentation */
ActorRef actorName: ActorClass1
/** A replicated actor of size 4 */
ActorRef actorName[4]: ActorClass1
/** A dynamic actor */
optional ActorRef actorName[*]: ActorClass1
```