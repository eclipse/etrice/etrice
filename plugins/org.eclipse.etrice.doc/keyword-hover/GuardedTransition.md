The guarded transition is used by data driven state machines to trigger state transitions

```room
Transition tr: running -> running {
	guard '''data_counter_finish.finish == true && data_finish_flag == false'''
	action '''++counter;'''
}
```