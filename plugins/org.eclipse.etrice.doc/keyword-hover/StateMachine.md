A StateMachine describes the state based, event driven behavior of an ActorClass

Open the **graphical diagram editor**:
		
* Press `ALT+B` (having focus on an *ActorClass*)
* Outline view -> Right click on an *ActorClass* -> Edit Behavior

```room
/** Documentation */
StateMachine {
	...
}
```