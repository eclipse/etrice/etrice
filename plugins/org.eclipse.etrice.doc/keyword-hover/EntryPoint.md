An entry point is an explicit entry point in a sub state machine to which transitions in the parent state graph can connect

```room
	StateMachine {
		State state0 {
			subgraph {
				State state0
				EntryPoint ep0
				ExitPoint ep1
				Transition tr0: my ep0 -> state0
				Transition tr1: state0 -> my ep1 {
					triggers {
						<in1: p0>
					}
				}
			}
		}
		State state1
		Transition init0: initial -> ep0 of state0
		Transition tr0: ep1 of state0 -> state1
	}
```