A transition point is the starting point of transitions that trigger for any state of this state machine

```room
StateMachine {
	TransitionPoint tp1
	Transition tr1: my tp1 -> my tp1 {
		triggers {
			<msg: port>
		}
	}
}
```