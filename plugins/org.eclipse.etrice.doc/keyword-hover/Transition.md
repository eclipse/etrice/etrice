A Transition is an edge in the state graph representation of the state machine

```room
Transition init: initial -> Standby
Transition tr0: Standby -> Running {
	triggers {
		<timeout: to>
	}
}
```