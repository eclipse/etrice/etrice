An EnumerationType declares an enumeration similar to most well-known languages

```room
Enumeration EOnOff {
	Off,
	On
}
Enumeration EOnOff of int8 {
	Off = 0,
	On = 1
}

/** Documentation */
Enumeration EnumName of PrimitiveType1 {
	// name1 = int
	// ...
}
```