A State is a node in the state graph representation of the state machine

```room
ActorClass Example {
	//...
	Behavior {
		StateMachine {
			Transition init: initial -> Standby
			Transition tr0: Standby -> Running {
				triggers {
					<timeout: to>
				}
			}
			Transition tr1: Running -> Stopped {
				triggers {
					<timeout: to>
				}
			}
			State Standby {
				entry '''
					to.startTimeout(200*10);'''
			}
			State Running {
				entry '''
					controllers.start();
					to.startTimeout(200*10);'''
			}
			State Stopped {
				entry '''controllers.stop();'''
			}
		}
	}
}
```