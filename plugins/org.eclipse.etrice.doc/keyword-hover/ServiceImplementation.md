The implementation of an Service Provision Point (SPP)

```room
Interface {
	SPP sppName: ProtocolClass1
}
Structure {
	ServiceImplementation of sppName
}
```