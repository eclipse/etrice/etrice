An Operation is a member function of a class

```room
/** A void operation */
Operation operationName() '''
	user code here;
	// don't forget the ';' at line end
'''
```