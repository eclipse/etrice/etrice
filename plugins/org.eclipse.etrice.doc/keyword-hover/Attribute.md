An Attribute is a member variable of a class

```room
/** a boolean attribute */
Attribute attributeName: boolean
/** an array attribute */
Attribute attributeName[8]: int32 = "0"
/** an external type attribute as reference */
Attribute attributeName: voidType ref = "NULL"
```