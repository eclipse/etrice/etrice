The LogicalSystem is the topmost structural class. It assembles a distributed system by means of sub systems

```room
/** Documentation */
LogicalSystem systemName {
	SubSystemRef ...
}
```