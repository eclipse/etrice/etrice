Protocols
=========

## Description

A ProtocolClass defines a set of incoming and outgoing Messages that can be exchanged between two ports. The exact semantics of a message is defined by the execution model.

## Motivation

-   Protocol classes provide a reusable interface specification for ports

-   Protocol classes can optionally specify valid message exchange sequences

## Notation

Protocol classes have only textual notation. The example defines a protocol class with 2 incoming and two outgoing messages. Messages can have data attached. The data can be of a primitive type (e.g. int32, float64, ...) or a data class.

```room
ProtocolClass ProtocolClass1 {
	incoming {
		Message m1(int32)
		Message m2()
	}
	outgoing {
		Message m3(DataClass1)
		Message m4()
	}
}
```

## Port Classes

Port classes are an advanced concept. They implement the *interceptor pattern* and allow the user to implement complex behavior as part of the ProtocolClass. A ProtocolClass may define port classes for both sides of a binding (regular and conjugated port class). 

Details can be found [here](../reference/room-language#portclass).
 
	