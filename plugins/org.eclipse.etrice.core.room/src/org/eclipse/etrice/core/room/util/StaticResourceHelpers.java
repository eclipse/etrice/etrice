/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		epaen (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core.room.util;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.eclipse.etrice.core.common.base.Annotation;
import org.eclipse.etrice.core.common.base.AnnotationType;
import org.eclipse.etrice.core.common.base.KeyValue;
import org.eclipse.etrice.core.resource.RoomResourceDescriptionStrategy;
import org.eclipse.etrice.core.room.ActorClass;
import org.eclipse.etrice.core.room.ActorContainerClass;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.IEObjectDescription;

import com.google.inject.Inject;

public class StaticResourceHelpers {
	public static final QualifiedName ANNOTATION_FQN_USES_RESOURCE = QualifiedName.create("etrice", "api",
			"annotations", "UsesResource");
	public static final String TYPE_IMPLICIT = "implicit";

	private static final Pattern PATTERN_USER_DATA = Pattern.compile(
			"(?:^|,)" + StaticResourceHelpers.ANNOTATION_FQN_USES_RESOURCE.toString().replace(".", "\\.") + ":");

	@Inject
	RoomHelpers roomHelpers;

	@Inject
	IQualifiedNameProvider qualifiedNames;

	/**
	 * Gets all UsesResources annotations with the specified type String for a
	 * particular container object, including any matching annotations in ancestor
	 * base containers.
	 *
	 * @param container target container object
	 * @param type      name of the UsesResource annotation type to match (matched
	 *                  with attribute names in the annotations)
	 * @return all matching UsesResource annotations from the target
	 *         ActorContainerClass and all of its base container classes
	 * @see #getResourcesOfType(ActorContainerClass, String)
	 */
	public Stream<Annotation> getTransitiveResourcesOfType(ActorContainerClass container, String type) {
		Collection<? extends ActorContainerClass> acList = getContainerHierarchy(container);
		return acList.stream().flatMap(ac -> getResourcesOfType(ac, type));
	}

	/**
	 * Gets all UsesResource annotations with the specified type String for a
	 * particular container object. Only directly contained annotations are
	 * considered, i.e. ancestor base containers are not checked.
	 *
	 * @param container target container object
	 * @param type      name of the UsesResource annotation type to match (matched
	 *                  with attribute names in the annotations)
	 * @return all matching UsesResource annotations directly contained in the
	 *         target ActorContainerClass
	 */
	public Stream<Annotation> getResourcesOfType(ActorContainerClass container, String type) {
		if (container == null) {
			return Stream.empty();
		}

		return roomHelpers.getAnnotations(container).stream().filter(annotation -> {
			AnnotationType annotationType = annotation.getType();
			return (annotationType != null) && (!annotationType.eIsProxy())
					&& (qualifiedNames.getFullyQualifiedName(annotationType).equals(ANNOTATION_FQN_USES_RESOURCE));
		}).filter(annotation -> isAttributeValid(annotation, type));
	}

	public boolean hasUsesResourceAnnotation(IEObjectDescription description) {
		String annotationUserData = description
				.getUserData(RoomResourceDescriptionStrategy.USER_DATA_ROOM_ANNOTATIONS);
		return (annotationUserData != null) && PATTERN_USER_DATA.matcher(annotationUserData).find();
	}

	private Collection<? extends ActorContainerClass> getContainerHierarchy(ActorContainerClass container) {
		RoomSwitch<Collection<? extends ActorContainerClass>> hierarchySwitch = new RoomSwitch<Collection<? extends ActorContainerClass>>() {
			@Override
			public Collection<? extends ActorContainerClass> caseActorClass(ActorClass object) {
				if (object.getActorBase() == null) {
					return Collections.singletonList(object);
				}

				return roomHelpers.getClassHierarchy(object);
			}

			@Override
			public List<? extends ActorContainerClass> caseActorContainerClass(ActorContainerClass object) {
				return Collections.singletonList(object);
			}
		};
		return hierarchySwitch.doSwitch(container);
	}

	/**
	 * Checks whether an attribute exists and whether its value is valid, i.e.
	 * non-null and resolved.
	 */
	private boolean isAttributeValid(Annotation annotation, String attributeName) {
		for (KeyValue attr : annotation.getAttributes()) {
			if ((attr.getKey() != null) && attr.getKey().equals(attributeName) && (attr.getValue() != null)
					&& !attr.getValue().eIsProxy()) {
				return true;
			}
		}
		return false;
	}
}
