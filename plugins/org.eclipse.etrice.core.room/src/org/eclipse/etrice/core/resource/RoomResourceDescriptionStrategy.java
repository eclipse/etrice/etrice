/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		epaen (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core.resource;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.etrice.core.common.base.AnnotationType;
import org.eclipse.etrice.core.common.base.KeyValue;
import org.eclipse.etrice.core.room.RoomPackage;
import org.eclipse.etrice.core.room.StructureClass;
import org.eclipse.etrice.core.room.util.RoomHelpers;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy;
import org.eclipse.xtext.util.IAcceptor;
import org.eclipse.xtext.util.Pair;
import org.eclipse.xtext.util.Tuples;

import com.google.inject.Inject;

/**
 * Adds StructureClass annotation information to the EObjectDescriptions
 * exported via ResourceDescriptions. Annotations are added to an
 * EObjectDescription user data entry as a comma-delimited list of (FQN,
 * attribute hash) pairs.
 */
public class RoomResourceDescriptionStrategy extends DefaultResourceDescriptionStrategy {

	private final static Logger LOG = Logger.getLogger(RoomResourceDescriptionStrategy.class);

	public final static String USER_DATA_ROOM_ANNOTATIONS = "ROOM_ANNOTATIONS";

	@Inject
	RoomHelpers roomHelpers;

	@Override
	public boolean createEObjectDescriptions(EObject eObject, IAcceptor<IEObjectDescription> acceptor) {
		if (!RoomPackage.eINSTANCE.getStructureClass().isInstance(eObject)) {
			return super.createEObjectDescriptions(eObject, acceptor);
		}

		if (getQualifiedNameProvider() == null)
			return false;

		if (((StructureClass) eObject).getAnnotations().isEmpty()) {
			return super.createEObjectDescriptions(eObject, acceptor);
		}

		// collect annotation information
		StructureClass sc = (StructureClass) eObject;
		Stream<String> annotationStrings = sc.getAnnotations().stream().map(annotation -> {
			AnnotationType type = annotation.getType();
			String annotationName = (type == null || type.eIsProxy()) ? "(unknown)"
					: getQualifiedNameProvider().getFullyQualifiedName(type).toString();
			int hash = getAttributesHash(annotation.getAttributes());
			return annotationName + ":" + hash;
		});

		try {
			QualifiedName qualifiedName = getQualifiedNameProvider().getFullyQualifiedName(eObject);
			if (qualifiedName != null) {
				Map<String, String> userData = Collections.singletonMap(USER_DATA_ROOM_ANNOTATIONS,
						annotationStrings.collect(Collectors.joining(",")));
				acceptor.accept(EObjectDescription.create(qualifiedName, eObject, userData));
			}
		} catch (Exception exc) {
			LOG.error(exc.getMessage(), exc);
		}
		return true;
	}

	private int getAttributesHash(Collection<KeyValue> attributes) {
		Collection<Pair<String, String>> raw = attributes.stream()
				.filter(it -> it.getKey() != null && it.getValue() != null && !it.getValue().eIsProxy())
				.map(it -> Tuples.pair(it.getKey(), roomHelpers.literalToString(it.getValue())))
				.collect(Collectors.toList());
		return Arrays.hashCode(raw.toArray());
	}
}
