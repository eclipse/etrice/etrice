/*******************************************************************************
 * Copyright (c) 2010 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * CONTRIBUTORS:
 * 		Thomas Schuetz and Henrik Rentz-Reichert (initial contribution)
 * 		Eyrak Paen
 *
 *******************************************************************************/


module org.eclipse.etrice.core.Room

import org.eclipse.xtext.xtext.generator.*
import org.eclipse.xtext.xtext.generator.model.project.*

var projectName = "org.eclipse.etrice.core.room"

Workflow {
    component = XtextGenerator {
    	configuration = {
    		project = StandardProjectConfig {
    			baseName = projectName
    			rootPath = ".."
    			runtimeTest = {
    				enabled = true
    				root = "../../tests/${projectName}.tests"
    			}
    			eclipsePlugin = {
    				enabled = true
    			}
    			createEclipseMetaData = true
    		}
    		code = {
				preferXtendStubs = false
    		}
    	}
    	language = StandardLanguage {
    		name = "org.eclipse.etrice.core.Room"
    		fileExtensions = "room"
    		referencedResource = "platform:/resource/org.eclipse.etrice.core.common/model/Base.genmodel"
    		referencedResource = "platform:/resource/org.eclipse.etrice.core.fsm/model/FSM.genmodel"
    		referencedResource = "platform:/resource/${projectName}/model/Room.genmodel"
    		serializer = {
    			generateStub = false
    		}
    		validator = {
    			// HOWTO: use URI imports - configure validator
                composedCheck = "org.eclipse.xtext.validation.ImportUriValidator"

                // HOWTO: extension manager is added here (CAUTION: don't register with RoomPackage in the class since this makes it is called twice)
                composedCheck = "org.eclipse.etrice.core.validation.ValidatorExtensionManager"
                composedCheck = "org.eclipse.etrice.core.validation.RoomNamesAreUniqueValidator"
                composedCheck = "org.eclipse.etrice.core.validation.InterfaceContractValidator"
                composedCheck = "org.eclipse.etrice.core.validation.WiringValidator"
                composedCheck = "org.eclipse.etrice.core.validation.StaticResourceValidator"
    		}
    		generator = {
				generateStub = false
			}
			scopeProvider = {
				generateXtendStub = true
			}
			formatter = {
				generateStub = true
				generateXtendStub = true
			}
			junitSupport = {
				generateStub = false
			}
    	}
    }
    // call the ecore generator only after running the Xtext generator because of directory cleaning issues and also this bug: https://bugs.eclipse.org/bugs/show_bug.cgi?id=550756
    component = org.eclipse.emf.mwe2.ecore.EcoreGenerator {
    	genModel = "platform:/resource/${projectName}/model/Room.genmodel"
    	srcPath = "platform:/resource/${projectName}/src-gen"
    }
}