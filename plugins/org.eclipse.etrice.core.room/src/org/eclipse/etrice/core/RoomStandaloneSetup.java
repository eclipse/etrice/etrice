/*******************************************************************************
 * Copyright (c) 2010 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Thomas Schuetz and Henrik Rentz-Reichert (initial contribution)
 * 
 *******************************************************************************/



package org.eclipse.etrice.core;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.etrice.core.room.RoomPackage;

import com.google.inject.Injector;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class RoomStandaloneSetup extends RoomStandaloneSetupGenerated{

	public static void doSetup() {
		new RoomStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
	
	@Override
	public void register(Injector injector) {
		if (!EPackage.Registry.INSTANCE.containsKey(RoomPackage.eNS_URI)) {
			EPackage.Registry.INSTANCE.put(RoomPackage.eNS_URI, RoomPackage.eINSTANCE);
		}
		super.register(injector);
	}
}

