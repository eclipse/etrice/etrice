/*******************************************************************************
 * Copyright (c) 2020 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Jan Belle (initial contribution)
 * 		Hakan Yesilay (Port Connection Warnings)
 *******************************************************************************/

package org.eclipse.etrice.core.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.etrice.core.room.ActorClass;
import org.eclipse.etrice.core.room.ActorContainerRef;
import org.eclipse.etrice.core.room.ActorRef;
import org.eclipse.etrice.core.room.Binding;
import org.eclipse.etrice.core.room.BindingEndPoint;
import org.eclipse.etrice.core.room.CommunicationType;
import org.eclipse.etrice.core.room.ConnectionNecessity;
import org.eclipse.etrice.core.room.LayerConnection;
import org.eclipse.etrice.core.room.LogicalSystem;
import org.eclipse.etrice.core.room.Port;
import org.eclipse.etrice.core.room.ProtocolClass;
import org.eclipse.etrice.core.room.ReferenceType;
import org.eclipse.etrice.core.room.RoomPackage;
import org.eclipse.etrice.core.room.SubSystemClass;
import org.eclipse.etrice.core.room.SubSystemRef;
import org.eclipse.etrice.core.room.util.Multiplicities;
import org.eclipse.etrice.core.room.util.RoomHelpers;
import org.eclipse.xtext.validation.AbstractDeclarativeValidator;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.validation.EValidatorRegistrar;

import com.google.inject.Inject;

/**
 * This validator enforces the constraints that are required by the wiring
 * algorithm. All relay ports must be implemented in the sense that they are
 * connected to as many end ports as their multiplicity implies. Further the
 * multiplicity of every end point must not be exceeded. The latter restriction
 * ensures that the wiring algorithm can act greedily while the former enables
 * the validation algorithm to consider every actor class individually.
 */
public class WiringValidator extends AbstractDeclarativeValidator {

	@Inject
	RoomHelpers roomHelpers;

	public static final String MULTIPLICITY_ANY_REQUIRES_OPTIONAL = "multiplicity any [*] requires optional";

	public static final String ACTOR_REF_CHANGE_REF_TYPE_TO_OPTIONAL = "MultiplicityValidator.ActorRefChangeRefTypeToOptional";

	@Check
	public void checkDataDrivenPortMultiplicity(Port port) {
		if (port.getProtocol() instanceof ProtocolClass
				&& ((ProtocolClass) port.getProtocol()).getCommType() == CommunicationType.DATA_DRIVEN
				&& port.getMultiplicity() != 1)
			error("multiplicity must be 1 for data driven ports", RoomPackage.eINSTANCE.getPort_Multiplicity());
	}

	@Check
	public void checkActorRefMultiplicity(ActorRef ref) {
		if (ref.getMultiplicity() == -1 && ref.getRefType() != ReferenceType.OPTIONAL)
			error(MULTIPLICITY_ANY_REQUIRES_OPTIONAL, RoomPackage.eINSTANCE.getActorRef_RefType(),
					ACTOR_REF_CHANGE_REF_TYPE_TO_OPTIONAL);
		if (ref.getMultiplicity() != 1) {
			ActorClass ac = ref.getType();
			if (getAll(ac, ActorClass::getInterfacePorts).stream().anyMatch(port -> port.getMultiplicity() == -1))
				error("replicated actors must not have replicated ports with multiplicity any", null);
			if (getAll(ac, ActorClass::getServiceProvisionPoints).stream().anyMatch(spp -> true))
				error("replicated actors must not have service provision points", null);
		}
	}

	@Check
	public void checkLayerConnectionTarget(LayerConnection lc) {
		if (lc.getTo().getRef() instanceof ActorRef)
			if (((ActorRef) lc.getTo().getRef()).getMultiplicity() > 1)
				error("layer connection must not connect to replicated actor", null);
	}

	/**
	 * Check that no binding exposes a mandatory endpoint of the actor class structure as optional via a relay port.
	 * 
	 * @param binding the binding to check
	 */
	@Check
	public void checkOptionalRelayPortBinding(Binding binding) {
		EndPoint ep1 = new EndPoint(binding.getEndpoint1());
		EndPoint ep2 = new EndPoint(binding.getEndpoint2());
		boolean isEp1Relay = ep1.ref == null && roomHelpers.isRelay(ep1.port);
		boolean isEp2Relay = ep2.ref == null && roomHelpers.isRelay(ep2.port);
		// only bindings from a port of an actor ref to a relay port are relevant (internal ports can't be relayed)
		if((ep1.ref != null && isEp2Relay) || (ep2.ref != null && isEp1Relay)) {
			Port relayPort = isEp1Relay ? ep1.port : ep2.port;
			EndPoint otherEp = isEp1Relay ? ep2 : ep1;
			// show a warning if the binding forwards a mandatory endpoint of the actor class structure to an optional relay port
			if(checkMandatoryKeyword(otherEp.port) && !checkMandatoryKeyword(relayPort)) {
				// only offer the quickfix if the relay port and the binding are defined in the same class
				boolean isRelayInherited = roomHelpers.getStructureClass(relayPort) != roomHelpers.getStructureClass(binding);
				String quickfixCode = isRelayInherited ? null : RoomValidator.CHANGE_CONNECTION_NECESSITY;
				warning("Relay port \"" + relayPort.getName() + "\" is connected to mandatory port \"" + otherEp.toString()+ "\" which requires a connection but the relay port is not defined as \"mandatory Port\"",
						null, quickfixCode, relayPort.getName());
			}
		}
	}

	@Check
	public void checkPortMultiplicities(LogicalSystem ls) {
		List<EndPoint> endpoints = new ArrayList<EndPoint>();
		for (SubSystemRef ref: ls.getSubSystems()) {
			List<Port> relayPorts = ref.getType().getRelayPorts();
			for (Port port: relayPorts) {
				EndPoint ep = new EndPoint(ref, port);
				endpoints.add(ep);
			}
		}
		checkPortMultiplicities(true, ls.getBindings(), Collections.emptyList(), endpoints);
	}

	@Check
	public void checkPortMultiplicities(SubSystemClass ssc) {
		List<Port> relayPorts = ssc.getRelayPorts();
		List<EndPoint> endpoints = new ArrayList<EndPoint>();
		for(Port port: relayPorts) {
			EndPoint ep = new EndPoint(port);
			endpoints.add(ep);
		}
		for (ActorRef ref: ssc.getActorRefs()) {
			List<Port> interfacePorts =  getAll(ref.getType(), ActorClass::getInterfacePorts);
			for (Port port: interfacePorts) {
				EndPoint ep = new EndPoint(ref, port);
				endpoints.add(ep);
			}
		}
		checkPortMultiplicities(true, ssc.getBindings(), relayPorts, endpoints);
	}

	@Check
	public void checkPortMultiplicities(ActorClass ac) {
		List<Port> relayPorts =  getAll(ac, ActorClass::getRelayPorts);
		List<EndPoint> endpoints = new ArrayList<EndPoint>();

		List<Port> internalPorts = getAll(ac, ActorClass::getInternalPorts);
		for(Port port: internalPorts) {
			EndPoint ep = new EndPoint(port);
			endpoints.add(ep);
		}
		
		for(Port port: relayPorts) {
			EndPoint ep = new EndPoint(port);
			endpoints.add(ep);
		}
		
		for (ActorRef ref: getAll(ac, ActorClass::getActorRefs)) {
			List<Port> interfacePorts = getAll(ref.getType(), ActorClass::getInterfacePorts);
			for (Port port: interfacePorts) {
				EndPoint ep = new EndPoint(ref, port);
				endpoints.add(ep);
			}
		}
		checkPortMultiplicities(!ac.isAbstract(), getAll(ac, ActorClass::getBindings), relayPorts, endpoints);
	}

	/**
	 * Computes the required multiplicity for each end point using the given
	 * bindings and then compares them to the actual multiplicity of the end points.
	 * @param checkForMandatoryBindings
	 * 			  to exclude abstract class from port connection check
	 * @param bindings
	 *            all bindings to consider when computing the required
	 *            multiplicities
	 * @param relays
	 *            all relay ports to check for implementation
	 * @param endpoints
	 * 			  all endpoints to check whether they are connected or not
	 */
	private void checkPortMultiplicities(boolean checkForMandatoryBindings, List<Binding> bindings, List<Port> relays, List<EndPoint> endpoints) {
		//initialize every multiplicity as zero
		Map<EndPoint, Integer> computedPeerCount = new HashMap<>();
		endpoints.forEach(ep -> {
			computedPeerCount.put(ep, 0);
		});
		
		bindings.forEach(binding -> {
			EndPoint ep1 = new EndPoint(binding.getEndpoint1());
			EndPoint ep2 = new EndPoint(binding.getEndpoint2());
			int multiplicity = Multiplicities.minimum(getMultiplicity(ep1), getMultiplicity(ep2));
			computedPeerCount.merge(ep1, multiplicity, Multiplicities::plus);
			computedPeerCount.merge(ep2, multiplicity, Multiplicities::plus);
		});

		computedPeerCount.entrySet().forEach(entry -> { 
			EndPoint ep = entry.getKey();
			int multiplicity = getMultiplicity(ep);
			int calculated = entry.getValue();
			if (checkForMandatoryBindings && Multiplicities.compare(calculated, multiplicity) < 0) {
				// Check all relay ports that is defined in the ActorClass (Internal connection of the relay ports should always be checked)
				if (relays.contains(ep.port) && ep.ref == null) {
					if (calculated == 0) {
						warning("Relay port \"" + ep.toString() + "\" is not connected.\nRelay ports must be connected internally.",
						RoomPackage.eINSTANCE.getRoomClass_Name());
					}
					else {
						warning("Relay port \"" + ep.toString() + "\" is connected to " + Multiplicities.toString(calculated)
								+ " peers but its multiplicity is " + Multiplicities.toString(multiplicity) + ". \nFor replicated relay ports each port must have a peer.",
								RoomPackage.eINSTANCE.getRoomClass_Name());
					}
				}
				// Check whether the multiplicity of any end point is reached for mandatory Ports
				else if (checkMandatoryKeyword(ep.port)) {
					if (calculated == 0) {
						warning("mandatory port \"" + ep.toString() + "\" is not connected",
						RoomPackage.eINSTANCE.getRoomClass_Name());
					}
					else {
						warning("mandatory port \"" + ep.toString() + "\" is connected to " + Multiplicities.toString(calculated)
								+ " peers but its multiplicity is " + Multiplicities.toString(multiplicity) + ". \nFor mandatory ports with multiplicity > 1 each port must have a peer.",
								RoomPackage.eINSTANCE.getRoomClass_Name());
					}
				}
			}
			// Check whether the multiplicity of any end point is exceeded.
			else if (Multiplicities.compare(calculated, multiplicity) > 0) {
				if (multiplicity == 1) {
					warning("port \"" + ep.toString() + "\" is connected to more than one peer",
							RoomPackage.eINSTANCE.getRoomClass_Name());
				}
				else {
					warning("port \"" + ep.toString() + "\" is connected to " + Multiplicities.toString(calculated)
							+ " peers but its multiplicity is only " + Multiplicities.toString(multiplicity),
							RoomPackage.eINSTANCE.getRoomClass_Name());
				}
			}
		});
	} 

	/**
	 * Computes the multiplicity of an end point. If the end point is part of a
	 * replicated actor reference, the multiplicity of the end point is the product
	 * of the multiplicity of the port and the actor reference. Otherwise the
	 * multiplicity of the end point is just the multiplicity of the port.
	 * 
	 * @param ep
	 *            the end point to compute the multiplicity of
	 * @return the multiplicity of the end point
	 */
	private int getMultiplicity(EndPoint ep) {
		int multiplicator = ep.ref != null && ep.ref instanceof ActorRef ? ((ActorRef) ep.ref).getMultiplicity() : 1;
		return Multiplicities.times(multiplicator, ep.port.getMultiplicity());
	}

	/**
	 * Collects all specified items of an actor class including inherited ones. This
	 * function terminates even if the class hierarchy is circular.
	 * 
	 * @param <T>
	 *            the type of the items
	 * @param ac
	 *            the actor class to collect the items of
	 * @param getter
	 *            a method reference to the getter of the items to be collected
	 * @return the collected items
	 */
	private <T> List<T> getAll(ActorClass ac, Function<ActorClass, Collection<? extends T>> getter) {
		return roomHelpers.getClassHierarchy(ac).stream().flatMap(getter.andThen(Collection::stream)).collect(Collectors.toList());
	}

	/**
	 * This class allows to use end points as keys in a {@link Map}.
	 * 
	 * @see BindingEndPoint
	 */
	private static class EndPoint {
		public final ActorContainerRef ref; // nullable
		public final Port port;

		public EndPoint(Port port) {
			this.ref = null;
			this.port = port;
		}

		public EndPoint(ActorContainerRef ref, Port port) {
			this.ref = ref;
			this.port = port;
		}

		public EndPoint(BindingEndPoint ep) {
			this(ep.getActorRef(), ep.getPort());
		}

		@Override
		public String toString() {
			return ref == null ? port.getName() : ref.getName() + "." + port.getName();
		}

		@Override
		public int hashCode() {
			return Objects.hash(port, ref);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EndPoint other = (EndPoint) obj;
			return Objects.equals(port, other.port) && Objects.equals(ref, other.ref);
		}
	}

	@Override
	public void register(EValidatorRegistrar registrar) {
		// library validator is not registered for a specific language
	}
		
	public boolean checkMandatoryKeyword (Port port) {
		return port.getConnectionNecessity() == ConnectionNecessity.MANDATORY;
	}
	
}